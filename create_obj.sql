
create table xx_log(
id  number,
msg varchar2(4000)
);

create or replace procedure xx_log_msg(p_msg varchar2)
is
  PRAGMA AUTONOMOUS_TRANSACTION;
begin
  insert into xx_log(id,msg) values (nvl((select max(id)+1 from xx_log),1),p_msg);
  commit;
  null;
end;
/


create or replace package WF_ENGINE AUTHID CURRENT_USER as
/* $Header: wfengs.pls 120.8.12020000.2 2012/10/02 21:28:18 alsosa ship $ */
/*#
 * Provides APIs that can be called by an application program
 * or a workflow function in the runtime phase to communicate
 * with the Workflow Engine and to change the status of
 * workflow process activities.
 * @rep:scope public
 * @rep:product OWF
 * @rep:displayname Workflow Engine
 * @rep:lifecycle active
 * @rep:compatibility S
 * @rep:category BUSINESS_ENTITY WF_ENGINE
 * @rep:ihelp FND/@eng_api See the related online help
 */
--
-- Constant values
--
threshold number := 50;    -- Cost over which to defer activities
debug boolean := FALSE;    -- Run engine in debug or normal mode

-- Standard date format string.  Used to convert dates to strings
-- when returning date values as activity function results.
date_format varchar2(30) := 'YYYY/MM/DD HH24:MI:SS';

-- Set_Context context
--   Process for which set_context function has been called
setctx_itemtype varchar2(8) := '';  -- Current itemtype
setctx_itemkey varchar2(240) := ''; -- Current itemkey

-- Post-Notification Function Context areas
--   Used to pass information to callback functions
context_nid number := '';          -- Notification id (if applicable)
context_text varchar2(2000) := ''; -- Text information

-- Bug 3065814
-- Global context variables for post-notification
context_user  varchar2(320);
context_user_comment VARCHAR2(4000);
context_recipient_role varchar2(320);
context_original_recipient varchar2(320);
context_from_role varchar2(320);
context_new_role   varchar2(320);
context_more_info_role  varchar2(320);
context_user_key varchar2(240);
context_proxy varchar2(320);

-- Bug 2156047
-- Global variables to store Notification id and
-- text information for Post Notification Function
g_nid number;          -- current notification id
g_text varchar2(2000); -- text information

-- Activity types
eng_process         varchar2(8) := 'PROCESS';  -- Process type activity
eng_function        varchar2(8) := 'FUNCTION'; -- Function type activity
eng_notification    varchar2(8) := 'NOTICE';   -- Notification type activity
-- eng_event           varchar2(8) := 'EVENT';    -- Event activity

-- Item activity statuses
eng_completed       varchar2(8) := 'COMPLETE'; -- Normal completion
eng_active          varchar2(8) := 'ACTIVE';   -- Activity running
eng_waiting         varchar2(8) := 'WAITING';  -- Activity waiting to run
eng_notified        varchar2(8) := 'NOTIFIED'; -- Notification open
eng_suspended       varchar2(8) := 'SUSPEND';  -- Activity suspended
eng_deferred        varchar2(8) := 'DEFERRED'; -- Activity deferred
eng_error           varchar2(8) := 'ERROR';    -- Completed with error

-- Standard activity result codes
eng_exception       varchar2(30) := '#EXCEPTION'; -- Unhandled exception
eng_timedout        varchar2(30) := '#TIMEOUT';   -- Activity timed out
eng_stuck           varchar2(30) := '#STUCK';     -- Stuck process
eng_force           varchar2(30) := '#FORCE';     -- Forced completion
eng_noresult        varchar2(30) := '#NORESULT';  -- No result for activity
eng_mail            varchar2(30) := '#MAIL';      -- Notification mail error
eng_null            varchar2(30) := '#NULL';      -- Noop result
eng_nomatch         varchar2(30) := '#NOMATCH';   -- Voting no winner
eng_tie             varchar2(30) := '#TIE';       -- Voting tie
eng_noskip          varchar2(30) := '#NOSKIP';    -- Skip not allowed

-- Activity loop reset values
eng_reset           varchar2(8) := 'RESET';  -- Loop with cancelling
eng_ignore          varchar2(8) := 'IGNORE'; -- Do not reset activity
eng_loop            varchar2(8) := 'LOOP';   -- Loop without cancelling

-- Start/end activity flags
eng_start           varchar2(8) := 'START'; -- Start activity
eng_end             varchar2(8) := 'END';   -- End activity

-- Function activity modes
eng_run             varchar2(8) := 'RUN';      -- Run mode
eng_cancel          varchar2(8) := 'CANCEL';   -- Cancel mode
eng_timeout         varchar2(8) := 'TIMEOUT';  -- Timeout mode
eng_setctx          varchar2(8) := 'SET_CTX';  -- Selector set context mode
eng_testctx         varchar2(8) := 'TEST_CTX'; -- Selector test context mode

-- HandleError command modes
eng_retry           varchar2(8) := 'RETRY'; -- Retry errored activity
eng_skip            varchar2(8) := 'SKIP';  -- Skip errored activity

eng_wferror         varchar2(8) := 'WFERROR'; -- Error process itemtype

-- Monitor access key names
wfmon_mon_key       varchar2(30) := '.MONITOR_KEY'; -- Read-only monitor key
wfmon_acc_key       varchar2(30) := '.ADMIN_KEY';  -- Admin monitor key

-- Schema attribute name
eng_schema          varchar2(320) := '#SCHEMA';  -- current schema name

-- Special activity attribute names
eng_priority         varchar2(30) := '#PRIORITY'; -- Priority override
eng_timeout_attr     varchar2(30) := '#TIMEOUT'; -- Priority override

-- Standard activity transitions
eng_trans_default    varchar2(30) := '*';
eng_trans_any        varchar2(30) := '#ANY';

-- commit for every n iterations
commit_frequency     number       := 500;

/**** #### Defined below *****/

-- Applications context flag
-- By default we want context to be preserved.
preserved_context    boolean      := TRUE;

 -- Execution Counter
 --
    ExecCounter number := 0;

--
-- Synch mode
--   NOTE: Synch mode is only to be used for in-line processes that are
--   run to completion and purged within one session.  Some process data
--   is never saved to the database, so the monitor, reports, any external
--   access to workflow tables, etc, will not work.
--
--   This mode is enabled by setting the user_key of the item to
--   wf_engine.eng_synch.
--
--   *** Do NOT enable this mode unless you are sure you understand
--   *** the implications!
--
synch_mode boolean := FALSE; -- *** OBSOLETE! DO NOT USE! ***

eng_synch varchar2(8) := '#SYNCH';

-- 16-DEC-03 shanjgik bug fix 2722369 Reassign modes added
eng_reassign varchar2(8) := 'REASSIGN';-- not exactly a mode, added just for completeness
eng_delegate varchar2(8) := 'DELEGATE';
eng_transfer varchar2(8) := 'TRANSFER';

type AttrRecTyp is record (
  name varchar2(30),
  text_value varchar2(4000),
  number_value number,
  date_value date);
type AttrArrayTyp is table of AttrRecTyp
index by binary_integer;

synch_attr_arr AttrArrayTyp;       -- Array of item attributes
synch_attr_count pls_integer := 0; -- Array size

type NameTabTyp is table of Wf_Item_Attribute_Values.NAME%TYPE
  index by binary_integer;
type TextTabTyp is table of Wf_Item_Attribute_Values.TEXT_VALUE%TYPE
  index by binary_integer;
type NumTabTyp is table of Wf_Item_Attribute_Values.NUMBER_VALUE%TYPE
  index by binary_integer;
type DateTabTyp is table of Wf_Item_Attribute_Values.DATE_VALUE%TYPE
  index by binary_integer;


--
-- AddItemAttr (PUBLIC)
--   Add a new unvalidated run-time item attribute.
-- IN:
--   itemtype - item type
--   itemkey - item key
--   aname - attribute name
--   text_value   - add text value to it if provided.
--   number_value - add number value to it if provided.
--   date_value   - add date value to it if provided.
-- NOTE:
--   The new attribute has no type associated.  Get/set usages of the
--   attribute must insure type consistency.
--
/*#
 * Adds a new item type attribute variable to the process. Although most item
 * type attributes are defined at design time, you can create new attributes at
 * runtime for a specific process. You can optionally set a default text,
 * number, or date value for a new item type attribute when the attribute is
 * created.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param aname Attribute Name
 * @param text_value Text Attribute Value
 * @param number_value Number Attribute Value
 * @param date_value Date Attribute Value
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Add Item Attribute
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_addp See the related online help
 */
procedure AddItemAttr(itemtype in varchar2,
                      itemkey in varchar2,
                      aname in varchar2,
                      text_value   in varchar2 default null,
                      number_value in number   default null,
                      date_value   in date     default null);

--
-- AddItemAttrTextArray (PUBLIC)
--   Add an array of new unvalidated run-time item attributes of type text.
-- IN:
--   itemtype - item type
--   itemkey - item key
--   aname - Array of Names
--   avalue - Array of New values for attribute
-- NOTE:
--   The new attributes have no type associated.  Get/set usages of these
--   attributes must insure type consistency.
--
/*#
 * Adds an array of new text item type attributes to the process. Although
 * most item type attributes are defined at design time, you can create new
 * attributes at runtime for a specific process. Use this API rather than the
 * AddItemAttr API for improved performance when you need to add large numbers
 * of text item type attributes at once.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param aname Attribute Name Array
 * @param avalue Attribute Value Array
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Add Text Item Attribute Value Array
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_aiaa See the related online help
 */
procedure AddItemAttrTextArray(
  itemtype in varchar2,
  itemkey  in varchar2,
  aname    in Wf_Engine.NameTabTyp,
  avalue   in Wf_Engine.TextTabTyp);

--
-- AddItemAttrNumberArray (PUBLIC)
--   Add an array of new unvalidated run-time item attributes of type number.
-- IN:
--   itemtype - item type
--   itemkey - item key
--   aname - Array of Names
--   avalue - Array of New values for attribute
-- NOTE:
--   The new attributes have no type associated.  Get/set usages of these
--   attributes must insure type consistency.
--
/*#
 * Adds an array of new number item type attributes to the process. Although
 * most item type attributes are defined at design time, you can create new
 * attributes at runtime for a specific process. Use this API rather than the
 * AddItemAttr API for improved performance when you need to add large numbers
 * of number item type attributes at once.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param aname Attribute Name Array
 * @param avalue Attribute Value Array
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Add Number Item Attribute Value Array
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_aiaa See the related online help
 */
procedure AddItemAttrNumberArray(
  itemtype in varchar2,
  itemkey  in varchar2,
  aname    in Wf_Engine.NameTabTyp,
  avalue   in Wf_Engine.NumTabTyp);

--
-- AddItemAttrDateArray (PUBLIC)
--   Add an array of new unvalidated run-time item attributes of type date.
-- IN:
--   itemtype - item type
--   itemkey - item key
--   aname - Array of Names
--   avalue - Array of New values for attribute
-- NOTE:
--   The new attributes have no type associated.  Get/set usages of these
--   attributes must insure type consistency.
--
/*#
 * Adds an array of new date item type attributes to the process. Although
 * most item type attributes are defined at design time, you can create new
 * attributes at runtime for a specific process. Use this API rather than the
 * AddItemAttr API for improved performance when you need to add large numbers
 * of date item type attributes at once.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param aname Attribute Name Array
 * @param avalue Attribute Value Array
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Add Date Item Attribute Value Array
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_aiaa See the related online help
 */
procedure AddItemAttrDateArray(
  itemtype in varchar2,
  itemkey  in varchar2,
  aname    in Wf_Engine.NameTabTyp,
  avalue   in Wf_Engine.DateTabTyp);

--
-- SetItemAttrText (PUBLIC)
--   Set the value of a text item attribute.
--   If the attribute is a NUMBER or DATE type, then translate the
--   text-string value to a number/date using attribute format.
--   For all other types, store the value directly.
-- IN:
--   itemtype - Item type
--   itemkey - Item key
--   aname - Attribute Name
--   avalue - New value for attribute
--
/*#
 * Sets the value of a text item type attribute in a process. You can
 * also use this API for attributes of type role, form, URL, lookup, or
 * document.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param aname Attribute Name
 * @param avalue Attribute Value
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Set Text Item Attribute Value
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_setp See the related online help
 */
procedure SetItemAttrText(itemtype in varchar2,
                          itemkey in varchar2,
                          aname in varchar2,
                          avalue in varchar2);

--
-- SetItemAttrText2 (PRIVATE)
--   Set the value of a text item attribute.
--   If the attribute is a NUMBER or DATE type, then translate the
--   text-string value to a number/date using attribute format.
--   For all other types, store the value directly.
-- IN:
--   p_itemtype - Item type
--   p_itemkey - Item key
--   p_aname - Attribute Name
--   p_avalue - New value for attribute
-- RETURNS:
--   boolean
--
function SetItemAttrText2(p_itemtype in varchar2,
                          p_itemkey in varchar2,
                          p_aname in varchar2,
                          p_avalue in varchar2) return boolean;

--
-- SetItemAttrNumber (PUBLIC)
--   Set the value of a number item attribute.
--   Attribute must be a NUMBER-type attribute.
-- IN:
--   itemtype - Item type
--   itemkey - Item key
--   aname - Attribute Name
--   avalue - New value for attribute
--
/*#
 * Sets the value of a number item type attribute in a process.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param aname Attribute Name
 * @param avalue Attribute Value
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Set Number Item Attribute Value
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_setp See the related online help
 */
procedure SetItemAttrNumber(itemtype in varchar2,
                            itemkey in varchar2,
                            aname in varchar2,
                            avalue in number);

--
-- SetItemAttrDate (PUBLIC)
--   Set the value of a date item attribute.
--   Attribute must be a DATE-type attribute.
-- IN:
--   itemtype - Item type
--   itemkey - Item key
--   aname - Attribute Name
--   avalue - New value for attribute
--
/*#
 * Sets the value of a date item type attribute in a process.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param aname Attribute Name
 * @param avalue Attribute Value
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Set Date Item Attribute Value
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_setp See the related online help
 */
procedure SetItemAttrDate(itemtype in varchar2,
                          itemkey in varchar2,
                          aname in varchar2,
                          avalue in date);

--
-- SetItemAttrDocument (PUBLIC)
--   Set the value of a document item attribute.
-- IN:
--   itemtype - Item type
--   itemkey - Item key
--   aname - Attribute Name
--   documentid - Document Identifier - full concatenated document attribute
--                strings:
--                nodeid:libraryid:documentid:version:document_name
--
/*#
 * Sets the value of an item attribute of type document, to a document
 * identifier.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param aname Attribute Name
 * @param documentid Document ID
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Set Document Item Attribute Value
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_siad See the related online help
 */
procedure SetItemAttrDocument(itemtype in varchar2,
                              itemkey in varchar2,
                              aname in varchar2,
                              documentid in varchar2);

--
-- SetItemAttrTextArray (PUBLIC)
--   Set the values of an array of text item attribute.
--   Unlike SetItemAttrText(), it stores the values directly.
-- IN:
--   itemtype - Item type
--   itemkey - Item key
--   aname - Array of Names
--   avalue - Array of New values for attribute
--
/*#
 * Sets the values of an array of item type attributes in a process. Use the
 * SetItemAttrTextArray API rather than the SetItemAttrText API for improved
 * performance when you need to set the values of large numbers of item type
 * attributes at once.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param aname Attribute Name Array
 * @param avalue Attribute Value Array
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Set Item Attribute Text Array
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_siaa See the related online help
 */
procedure SetItemAttrTextArray(
  itemtype in varchar2,
  itemkey  in varchar2,
  aname    in Wf_Engine.NameTabTyp,
  avalue   in Wf_Engine.TextTabTyp);

--
-- SetItemAttrNumberArray (PUBLIC)
--   Set the value of an array of number item attribute.
--   Attribute must be a NUMBER-type attribute.
-- IN:
--   itemtype - Item type
--   itemkey - Item key
--   aname - Array of Names
--   avalue - Array of new value for attribute
--
/*#
 * Sets the values of an array of item type attributes in a process. Use the
 * SetItemAttrNumberArray API rather than the SetItemAttrNumber API for improved
 * performance when you need to set the values of large numbers of item type
 * attributes at once.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param aname Attribute Name Array
 * @param avalue Attribute Value Array
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Set Item Attribute Number Array
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_siaa See the related online help
 */
procedure SetItemAttrNumberArray(
  itemtype in varchar2,
  itemkey  in varchar2,
  aname    in Wf_Engine.NameTabTyp,
  avalue   in Wf_Engine.NumTabTyp);

--
-- SetItemAttrDateArray (PUBLIC)
--   Set the value of an array of date item attribute.
--   Attribute must be a DATE-type attribute.
-- IN:
--   itemtype - Item type
--   itemkey - Item key
--   aname - Array of Name
--   avalue - Array of new value for attribute
--
/*#
 * Sets the values of an array of item type attributes in a process. Use the
 * SetItemAttrDateArray API rather than the SetItemAttrDate API for improved
 * performance when you need to set the values of large numbers of item type
 * attributes at once.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param aname Attribute Name Array
 * @param avalue Attribute Value Array
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Set Item Attribute Date Array
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_siaa See the related online help
 */
procedure SetItemAttrDateArray(
  itemtype in varchar2,
  itemkey  in varchar2,
  aname    in Wf_Engine.NameTabTyp,
  avalue   in Wf_Engine.DateTabTyp);

--
-- Getitemattrinfo (PUBLIC)
--   Get type information about a item attribute.
-- IN:
--   itemtype - Item type
--   aname - Attribute name
-- OUT:
--   atype  - Attribute type
--   subtype - 'SEND' or 'RESPOND'
--   format - Attribute format
--
/*#
 * Returns information about an item type attribute, such as its type and format,
 * if any is specified. Currently, subtype information is not available for item
 * type attributes
 * @param itemtype Item Type
 * @param aname Attribute Name
 * @param atype Attribute Type
 * @param subtype Attribute Subtype
 * @param format Attribute Format
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Get Item Attribute Information
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_getiainfo See the related online help
 */
procedure GetItemAttrInfo(itemtype in varchar2,
                          aname in varchar2,
                          atype out NOCOPY varchar2,
                          subtype out NOCOPY varchar2,
                          format out NOCOPY varchar2);

--
-- GetItemAttrText (PUBLIC)
--   Get the value of a text item attribute.
--   If the attribute is a NUMBER or DATE type, then translate the
--   number/date value to a text-string representation using attrbute format.
--   For all other types, get the value directly.
-- IN:
--   itemtype - Item type
--   itemkey - Item key
--   aname - Attribute Name
-- RETURNS:
--   Attribute value
--
/*#
 * Returns the value of a text item type attribute in a process. You can
 * also use this API for attributes of type role, form, URL, lookup, or
 * document.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param aname Attribute Name
 * @param ignore_notfound Ignore if Not Found
 * @return Text Attribute Value
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Get Text Item Attribute Value
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_getp See the related online help
 */
function GetItemAttrText(itemtype in varchar2,
                         itemkey in varchar2,
                         aname in varchar2,
                         ignore_notfound in boolean default FALSE)
return varchar2;

--
-- GetItemAttrNumber (PUBLIC)
--   Get the value of a number item attribute.
--   Attribute must be a NUMBER-type attribute.
-- IN:
--   itemtype - Item type
--   itemkey - Item key
--   aname - Attribute Name
-- RETURNS:
--   Attribute value
--
/*#
 * Returns the value of an item type number attribute in a process.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param aname Attribute Name
 * @param ignore_notfound Ignore if Not Found
 * @return Number Attribute Value
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Get Number Item Attribute Value
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_getp See the related online help
 */
function GetItemAttrNumber(itemtype in varchar2,
                           itemkey in varchar2,
                           aname in varchar2,
                           ignore_notfound in boolean default FALSE)
return number;

--
-- GetItemAttrDate (PUBLIC)
--   Get the value of a date item attribute.
--   Attribute must be a DATE-type attribute.
-- IN:
--   nid - Item id
--   aname - Attribute Name
-- RETURNS:
--   Attribute value
--
/*#
 * Returns the value of an item type date attribute in a process.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param aname Attribute Name
 * @param ignore_notfound Ignore if Not Found
 * @return Date Attribute Value
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Get Date Item Attribute Value
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_getp See the related online help
 */
function GetItemAttrDate (itemtype in varchar2,
                          itemkey in varchar2,
                          aname in varchar2,
                          ignore_notfound in boolean default FALSE)
return date;

--
-- GetItemAttrDocument (PUBLIC)
--   Get the value of a document item attribute.
-- IN:
--   itemtype - Item type
--   itemkey - Item key
--   aname - Attribute Name
-- RETURNS:
--   documentid - Document Identifier - full concatenated document attribute
--                strings:
--                nodeid:libraryid:documentid:version:document_name
--
--
--
/*#
 * Returns the document identifier for a DM document-type item attribute.
 * The document identifier is a concatenated string of the following values:
 * DM:<nodeid>:<documentid>:<version>
 * <nodeid> is the node ID assigned to the document management system node as
 * defined in the Document Management Nodes web page. <documentid> is the
 * document ID of the document, as assigned by the document management system
 * where the document resides. <version> is the version of the document. If a
 * version is not specified, the latest version is assumed.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param aname Attribute Name
 * @param ignore_notfound Ignore if Not Found
 * @return Document ID
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Get Document Item Attribute Value
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_giad See the related online help
 */
Function GetItemAttrDocument(itemtype in varchar2,
                              itemkey in varchar2,
                              aname in varchar2,
                              ignore_notfound in boolean default FALSE)
RETURN VARCHAR2;


--
-- GetActivityAttrInfo (PUBLIC)
--   Get type information about an activity attribute.
-- IN:
--   itemtype - Item type
--   itemkey - Item key
--   actid - Process activity id
--   aname - Attribute name
-- OUT:
--   atype  - Attribute type
--   subtype - 'SEND' or 'RESPOND',
--   format - Attribute format
--
/*#
 * Returns information about an activity attribute, such as its type and format,
 * if any is specified. This procedure currently does not return any subtype
 * information for activity attributes.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param actid Activity ID
 * @param aname Attribute Name
 * @param atype Attribute Type
 * @param subtype Attribute Subtype
 * @param format Attribute Format
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Get Activity Attribute Information
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_getaainfo See the related online help
 */
procedure GetActivityAttrInfo(itemtype in varchar2,
                              itemkey in varchar2,
                              actid in number,
                              aname in varchar2,
                              atype out NOCOPY varchar2,
                              subtype out NOCOPY varchar2,
                              format out NOCOPY varchar2);

--
-- GetActivityAttrText (PUBLIC)
--   Get the value of a text item attribute.
--   If the attribute is a NUMBER or DATE type, then translate the
--   number/date value to a text-string representation using attrbute format.
--   For all other types, get the value directly.
-- IN:
--   itemtype - Item type
--   itemkey - Item key
--   actid - Process activity id
--   aname - Attribute Name
-- RETURNS:
--   Attribute value
--
/*#
 * Returns the value of a text activity attribute in a process. You can
 * also use this API for attributes of type role, form, URL, lookup,
 * attribute, or document.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param actid Activity ID
 * @param aname Attribute Name
 * @param ignore_notfound Ignore if not found
 * @return Text Activity Attribute Value
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Get Text Activity Attribute Value
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_getaa See the related online help
 */
function GetActivityAttrText(itemtype in varchar2,
                             itemkey in varchar2,
                             actid in number,
                             aname in varchar2,
                             ignore_notfound in boolean default FALSE)
return varchar2;

--
-- GetActivityAttrNumber (PUBLIC)
--   Get the value of a number item attribute.
--   Attribute must be a NUMBER-type attribute.
-- IN:
--   itemtype - Item type
--   itemkey - Item key
--   actid - Process activity id
--   aname - Attribute Name
-- RETURNS:
--   Attribute value
--
/*#
 * Returns the value of a number activity attribute in a process.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param actid Activity ID
 * @param aname Attribute Name
 * @param ignore_notfound Ignore if not found
 * @return Number Activity Attribute Value
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Get Number Activity Attribute Value
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_getaa See the related online help
 */
function GetActivityAttrNumber(itemtype in varchar2,
                               itemkey in varchar2,
                               actid in number,
                               aname in varchar2,
                               ignore_notfound in boolean default FALSE)
return number;

--
-- GetActivityAttrDate (PUBLIC)
--   Get the value of a date item attribute.
--   Attribute must be a DATE-type attribute.
-- IN:
--   itemtype - Item type
--   itemkey - Item key
--   actid - Process activity id
--   aname - Attribute Name
-- RETURNS:
--   Attribute value
--
/*#
 * Returns the value of a date activity attribute in a process.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param actid Activity ID
 * @param aname Attribute Name
 * @param ignore_notfound Ignore if not found
 * @return Date Activity Attribute Value
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Get Date Activity Attribute Value
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_getaa See the related online help
 */
function GetActivityAttrDate(itemtype in varchar2,
                             itemkey in varchar2,
                             actid in number,
                             aname in varchar2,
                             ignore_notfound in boolean default FALSE)
return date;

--
-- Set_Item_Parent (PUBLIC)
-- *** OBSOLETE - Use SetItemParent instead ***
--
procedure Set_Item_Parent(itemtype in varchar2,
  itemkey in varchar2,
  parent_itemtype in varchar2,
  parent_itemkey in varchar2,
  parent_context in varchar2);

--
-- SetItemParent (PUBLIC)
--   Set the parent info of an item
-- IN
--   itemtype - Item type
--   itemkey - Item key
--   parent_itemtype - Itemtype of parent
--   parent_itemkey - Itemkey of parent
--   parent_context - Context info about parent
--
/*#
 * Defines the parent/child relationship for a master process and a detail
 * process. This API must be called by any detail process spawned from a
 * master process to define the parent/child relationship between the two
 * processes. You make a call to this API after you call the CreateProcess
 * API, but before you call the StartProcess API for the detail process.
 * @param itemtype Child Item Type
 * @param itemkey Child Item Key
 * @param parent_itemtype Parent Item Type
 * @param parent_itemkey Parent Item Key
 * @param parent_context Parent Context
 * @param masterdetail Master Detail Coordination
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Set Item Parent
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#parent See the related online help
 */
procedure SetItemParent(itemtype in varchar2,
  itemkey in varchar2,
  parent_itemtype in varchar2,
  parent_itemkey in varchar2,
  parent_context in varchar2,
  masterdetail   in boolean default NULL);

--
-- SetItemOwner (PUBLIC)
--   Set the owner of an item
-- IN
--   itemtype - Item type
--   itemkey - Item key
--   owner - Role designated as owner of the item
--
/*#
 * Sets the owner of existing items. The owner must be a valid role. Typically,
 * the role that initiates a transaction is assigned as the process owner, so
 * that any participant in that role can find and view the status of that
 * process instance in the Workflow Monitor.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param owner Item Owner Role
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Set Item Owner
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#itown See the related online help
 */
procedure SetItemOwner(
  itemtype in varchar2,
  itemkey in varchar2,
  owner in varchar2);

--
-- GetItemUserKey (PUBLIC)
--   Get the user key of an item
-- IN
--   itemtype - Item type
--   itemkey - Item key
-- RETURNS
--   User key of the item
--
/*#
 * Returns the user-friendly key assigned to an item in a process, identified by
 * an item type and item key. The user key is a user-friendly identifier to
 * locate items in the Workflow Monitor and other user interface components of
 * Oracle Workflow.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @return Item User Key
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Get Item User Key
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_getikey See the related online help
 */
function GetItemUserKey(
  itemtype in varchar2,
  itemkey in varchar2)
return varchar2;

--
-- SetItemUserKey (PUBLIC)
--   Set the user key of an item
-- IN
--   itemtype - Item type
--   itemkey - Item key
--   userkey - User key to be set
--
/*#
 * Sets a user-friendly identifier for an item in a process, which is initially
 * identified by an item type and item key. The user key is intended to be a
 * user-friendly identifier to locate items in the Workflow Monitor and other
 * user interface components of Oracle Workflow.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param userkey User Key
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Set Item User Key
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_setikey See the related online help
 */
procedure SetItemUserKey(
  itemtype in varchar2,
  itemkey in varchar2,
  userkey in varchar2);

--
-- GetActivityLabel (PUBLIC)
--  Get activity instance label given id, in a format
--  suitable for passing to other wf_engine apis.
-- IN
--   actid - activity instance id
-- RETURNS
--   <process_name>||':'||<instance_label>
--
/*#
 * Returns the instance label of an activity, given the internal activity
 * instance ID. The label returned has the following format, which is
 * suitable for passing to other Workflow Engine APIs, such as
 * CompleteActivity and HandleError, that accept activity labels as
 * arguments:
 *  <br> &lt;process_name&gt;:&lt;instance_label&gt;
 * @param actid Activity ID
 * @return Activity Label
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Get Activity Label
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_getlabel See the related online help
 */
function GetActivityLabel(
  actid in number)
return varchar2;

--
-- CB (PUBLIC)
--   This is the callback function used by the notification system to
--   get and set process attributes, and mark a process complete.
--
--   The command may be one of 'GET', 'SET', 'COMPLETE', or 'ERROR'.
--     GET - Get the value of an attribute
--     SET - Set the value of an attribute
--     COMPLETE - Mark the activity as complete
--     ERROR - Mark the activity as error status
--     TESTCTX - Test current context via selector function
--     FORWARD - Execute notification function for FORWARD
--     TRANSFER - Execute notification function for TRANSFER
--     RESPOND - Execute notification function for RESPOND
--
--   The context is in the format <itemtype>:<itemkey>:<activityid>.
--
--   The text_value/number_value/date_value fields are mutually exclusive.
--   It is assumed that only one will be used, depending on the value of
--   the attr_type argument ('VARCHAR2', 'NUMBER', or 'DATE').
--
-- IN:
--   command - Action requested.  Must be one of 'GET', 'SET', or 'COMPLETE'.
--   context - Context data in the form '<item_type>:<item_key>:<activity>'
--   attr_name - Attribute name to set/get for 'GET' or 'SET'
--   attr_type - Attribute type for 'SET'
--   text_value - Text Attribute value for 'SET'
--   number_value - Number Attribute value for 'SET'
--   date_value - Date Attribute value for 'SET'
-- OUT:
--   text_value - Text Attribute value for 'GET'
--   number_value - Number Attribute value for 'GET'
--   date_value - Date Attribute value for 'GET'
--
procedure CB(command in varchar2,
             context in varchar2,
             attr_name in varchar2 default null,
             attr_type in varchar2 default null,
             text_value in out NOCOPY varchar2,
             number_value in out NOCOPY number,
             date_value in out NOCOPY date);

-- Bug 2376033
--  Call back function with additional input parameter to get
--  value for an event attribute
-- IN
--   event_value - Event Attribute value for 'SET'
-- OUT
--   event_value - Event Attribute value for 'GET'

procedure CB(command in varchar2,
             context in varchar2,
             attr_name in varchar2 default null,
             attr_type in varchar2 default null,
             text_value in out NOCOPY varchar2,
             number_value in out NOCOPY number,
             date_value in out NOCOPY date,
             event_value in out nocopy wf_event_t);

--
-- ProcessDeferred (PUBLIC)
--   Process one deferred activity.
-- IN
--   itemtype - Item type to process.  If null process all item types.
--   minthreshold - Minimum cost activity to process. No minimum if null.
--   maxthreshold - Maximum cost activity to process. No maximum if null.
--
procedure ProcessDeferred(itemtype in varchar2 default null,
                          minthreshold in number default null,
                          maxthreshold in number default null);

--
-- ProcessTimeout (PUBLIC)
--  Pick up one timed out activity and execute timeout transition.
-- IN
--  itemtype - Item type to process.  If null process all item types.
--
procedure ProcessTimeOut( itemtype in varchar2 default null );

--
-- ProcessStuckProcess (PUBLIC)
--   Pick up one stuck process, mark error status, and execute error process.
-- IN
--   itemtype - Item type to process.  If null process all item types.
--
procedure ProcessStuckProcess(itemtype in varchar2 default null);

--
-- Background (PUBLIC)
--  Process all current deferred and/or timeout activities within
--  threshold limits.
-- IN
--   itemtype - Item type to process.  If null process all item types.
--   minthreshold - Minimum cost activity to process. No minimum if null.
--   maxthreshold - Maximum cost activity to process. No maximum if null.
--   process_deferred - Run deferred or waiting processes
--   process_timeout - Handle timeout process errors
--   process_stuck - Handle stuck process errors
--
/*#
 * Runs a background engine for processing deferred activities, timed out
 * activities, and stuck processes using the parameters specified. The
 * background engine executes all activities that satisfy the given arguments
 * at the time that the background engine is invoked. This procedure does not
 * remain running long term, so you must restart this procedure periodically.
 * Any activities that are newly deferred or timed out or processes that become
 * stuck after the current background engine starts are processed by the next
 * background engine that is invoked. You may run a script called wfbkgchk.sql
 * to get a list of the activities waiting to be processed by the next
 * background engine run.
 * @param itemtype Item Type
 * @param minthreshold Minimum Threshold
 * @param maxthreshold Maximum Threshold
 * @param process_deferred Process Deferred Activities
 * @param process_timeout Process Timeout Activities
 * @param process_stuck Process Stuck Activities
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Workflow Background Engine
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_bckgr See the related online help
 */
procedure Background (itemtype         in varchar2 default '',
                      minthreshold     in number default null,
                      maxthreshold     in number default null,
                      process_deferred in boolean default TRUE,
                      process_timeout  in boolean default TRUE,
                      process_stuck    in boolean default FALSE);

--
-- BackgroundConcurrent (PUBLIC)
--  Run background process for deferred and/or timeout activities
--  from Concurrent Manager.
--  This is a cover of Background() with different argument types to
--  be used by the Concurrent Manager.
-- IN
--   errbuf - CPM error message
--   retcode - CPM return code (0 = success, 1 = warning, 2 = error)
--   itemtype - Item type to process.  If null process all item types.
--   minthreshold - Minimum cost activity to process. No minimum if null.
--   maxthreshold - Maximum cost activity to process. No maximum if null.
--   process_deferred - Run deferred or waiting processes
--   process_timeout - Handle timeout errors
--   process_stuck - Handle stuck process errors
--
procedure BackgroundConcurrent (
    errbuf out NOCOPY varchar2,
    retcode out NOCOPY varchar2,
    itemtype in varchar2 default '',
    minthreshold in varchar2 default '',
    maxthreshold in varchar2 default '',
    process_deferred in varchar2 default 'Y',
    process_timeout in varchar2 default 'Y',
    process_stuck in varchar2 default 'N',
    instance_number in number default 0);

--
-- CreateProcess (PUBLIC)
--   Create a new runtime process (for an application item).
-- IN
--   itemtype - A valid item type
--   itemkey  - A string generated from the application object's primary key.
--   process  - A valid root process for this item type
--              (or null to use the item's selector function)
--   user_key - Optional parameter to avoid having to call SetItemUserKey later.
--   owner_role - Optional paramer to avoid having to call SetItemOwner later.
--
/*#
 * Creates a new runtime process for an application item. For example, a
 * Requisition item type may have a Requisition Approval Process as a top level
 * process. When a particular requisition is created, an application calls
 * CreateProcess to set up the information needed to start the defined process.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param process Process Name
 * @param user_key User Key
 * @param owner_role Item Owner Role
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Create Runtime Process
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_createp See the related online help
 */
procedure CreateProcess(itemtype in varchar2,
                        itemkey  in varchar2,
                        process  in varchar2 default '',
                        user_key in varchar2 default null,
                        owner_role in varchar2 default null);

--
-- StartProcess (PUBLIC)
--   Begins execution of the process. The process will be identified by the
--   itemtype and itemkey.  The engine locates the starting activities
--   of the root process and executes them.
-- IN
--   itemtype - A valid item type
--   itemkey  - A string generated from the application object's primary key.
--
/*#
 * Begins execution of the specified process. The engine locates the activity
 * marked as START and then executes it. CreateProcess() must first be called
 * to define the itemtype and itemkey before calling StartProcess().
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Start Process
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_startp See the related online help
 */
procedure StartProcess(itemtype in varchar2,
                       itemkey  in varchar2);


--
-- LaunchProcess (PUBLIC)
--   Launch a process both creates and starts it.
--   This is a wrapper for friendlier UI
-- IN
--   itemtype - A valid item type
--   itemkey  - A string generated from the application object's primary key.
--   process  - A valid root process for this item type
--              (or null to use the item's selector function)
--   userkey - User key to be set
--   owner - Role designated as owner of the item
--
/*#
 * Launches a specified process by creating the new runtime process and beginning
 * its execution. This is a wrapper that combines CreateProcess and StartProcess.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param process Process Name
 * @param userkey User Key
 * @param owner Item Owner Role
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Launch Process
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_launchp See the related online help
 */
procedure LaunchProcess(itemtype in varchar2,
                        itemkey  in varchar2,
                        process  in varchar2 default '',
                        userkey  in varchar2 default '',
                        owner    in varchar2 default '');



--
-- SuspendProcess (PUBLIC)
--   Suspends process execution, meaning no new transitions will occur.
--   Outstanding notifications will be allowed to complete, but they will not
--   cause activity transitions. If the process argument is null, the root
--   process for the item is suspended, otherwise the named process is
--   suspended.
-- IN
--   itemtype - A valid item type
--   itemkey  - A string generated from the application object's primary key.
--   process  - Process to suspend, specified in the form
--              [<parent process_name>:]<process instance_label>
--              If null suspend the root process.
--
/*#
 * Suspends process execution so that no new transitions occur. Outstanding
 * notifications can complete by calling CompleteActivity(), but the workflow
 * does not transition to the next activity. Restart suspended processes by
 * calling ResumeProcess().
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param process Process Name
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Suspend Process
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_suspnp See the related online help
 */
procedure SuspendProcess(itemtype in varchar2,
                         itemkey  in varchar2,
                         process  in varchar2 default '');


--
-- SuspendAll (PUBLIC)) --</rwunderl:1833759>
--   Suspends all processes for a given itemType.
-- IN
--   itemtype - A valid itemType
--   process
--

Procedure SuspendAll (p_itemType in varchar2,
                      p_process in varchar2 default NULL);

--
-- AbortProcess (PUBLIC)
--   Abort process execution. Outstanding notifications are canceled. The
--   process is then considered complete, with a status specified by the
--   result argument.
-- IN
--   itemtype - A valid item type
--   itemkey  - A string generated from the application object's primary key.
--   process  - Process to abort, specified in the form
--              [<parent process_name>:]<process instance_label>
--              If null abort the root process.
--   result   - Result to complete process with
--   verify_lock - TRUE if the item should be locked before processing it
--   cascade  - TRUE is you want to cascade purge all master-child
--              relations associated with this item
--
/*#
 * Aborts process execution and cancels outstanding notifications. The process
 * status is considered COMPLETE, with a result specified by the <code>result</code>
 * argument. Also, any outstanding notifications or subprocesses are set to a status of
 * COMPLETE with a result of force, regardless of the <code>result</code> argument.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param process Process Name
 * @param result Status of Aborted Process
 * @param verify_lock Lock Item Before Processing (TRUE/FALSE)
 * @param cascade Abort Associated Child Processes (TRUE/FALSE)
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Abort Process
 * @rep:compatibility S
 * @rep:businessevent oracle.apps.wf.engine.abort
 * @rep:ihelp FND/@eng_api#a_abortp See the related online help
 */
procedure AbortProcess(itemtype in varchar2,
                       itemkey  in varchar2,
                       process  in varchar2 default '',
                       result   in varchar2 default wf_engine.eng_force,
		       verify_lock in boolean default FALSE,
		       cascade  in boolean default FALSE);

--
-- ResumeProcess (PUBLIC)
--   Returns a process to normal execution status. Any transitions which
--   were deferred by SuspendProcess() will now be processed.
-- IN
--   itemtype   - A valid item type
--   itemkey    - A string generated from the application object's primary key.
--   process  - Process to resume, specified in the form
--              [<parent process_name>:]<process instance_label>
--              If null resume the root process.
--
/*#
 * Returns a suspended process to normal execution status. Any activities that
 * were transitioned to while the process was suspended are now executed.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param process Process Name
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Resume Process
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_resump See the related online help
 */
procedure ResumeProcess(itemtype in varchar2,
                        itemkey  in varchar2,
                        process  in varchar2 default '');


--
-- ResumeAll (PUBLIC) --</rwunderl:1833759>
--   Resumes all processes for a given itemType.
-- IN
--   itemtype - A valid itemType
--   process
--
Procedure ResumeAll (p_itemType in varchar2,
                     p_process  in varchar2 default NULL);


--
-- CreateForkProcess (PUBLIC)
--   Performs equivalent of createprocess but for a forked process
--   and copies all item attributes
--   If same version is false, this is same as CreateProcess but copies
--   item attributes as well.
-- IN
--   copy_itemtype  - Item type
--   copy_itemkey   - item key to copy (will be stored to an item attribute)
--   new_itemkey    - item key to create
--   same_version   - TRUE will use same version even if out of date.
--                    FALSE will use the active and current version
/*#
 * Forks a runtime process by creating a new process that is a copy of the
 * original. After calling CreateForkProcess(), you can call APIs such as
 * SetItemOwner(), SetItemUserKey(), or the SetItemAttribute APIs to reset
 * any item properties or modify any item attributes that you want for the
 * new process. Then you must call StartForkProcess() to start the new process.
 * @param copy_itemtype Original Item Type
 * @param copy_itemkey Original Item Key
 * @param new_itemkey New Item Key
 * @param same_version Same version as original item
 * @param masterdetail Master-Detail Process
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Create Fork Process
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_crfork See the related online help
 */
Procedure CreateForkProcess (
     copy_itemtype  in varchar2,
     copy_itemkey   in varchar2,
     new_itemkey    in varchar2,
     same_version   in boolean default TRUE,
     masterdetail   in boolean default NULL);

--
-- StartForkProcess (PUBLIC)
--   Start a process that has been forked. Depending on the way this was forked,
--   this will execute startprocess if its to start with the latest version or
--   it copies the forked process activty by activity.
-- IN
--   itemtype  - Item type
--   itemkey   - item key to start
--
/*#
 * Begins execution of the new forked process that you specify. Before you call
 * StartForkProcess(), you must first call CreateForkProcess() to create the
 * new process. You can modify the item attributes of the new process before
 * calling StartForkProcess().
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Start Fork Process
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_stfork See the related online help
 */
procedure StartForkProcess(
     itemtype        in  varchar2,
     itemkey         in  varchar2);

--
-- BeginActivity (PUBLIC)
--   Determines if the specified activity may currently be performed on the
--   work item. This is a test that the performer may proactively determine
--   that their intent to perform an activity on an item is, in fact, allowed.
-- IN
--   itemtype  - A valid item type
--   itemkey   - A string generated from the application object's primary key.
--   activity  - Completed activity, specified in the form
--               [<parent process_name>:]<process instance_label>
--
/*#
 * Determines if the specified activity can currently be performed on the
 * process item and raises an exception if it cannot.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param activity Activity Label to Begin
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Begin Activity
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_beginp See the related online help
 */
procedure BeginActivity(itemtype in varchar2,
                        itemkey  in varchar2,
                        activity in varchar2);

--
-- CompleteActivity (PUBLIC)
--   Notifies the workflow engine that an activity has been completed for a
--   particular process(item). This procedure can have one or more of the
--   following effects:
--   o Creates a new item. If the completed activity is the start of a process,
--     then a new item can be created by this call. If the completed activity
--     is not the start of a process, it would be an invalid activity error.
--   o Complete an activity with an optional result. This signals the
--     workflow engine that an asynchronous activity has been completed.
--     An optional activity completion result can also be passed.
-- IN
--   itemtype  - A valid item type
--   itemkey   - A string generated from the application object's primary key.
--   activity  - Completed activity, specified in the form
--               [<parent process_name>:]<process instance_label>
--   <result>  - An optional result.
--
/*#
 * Notifies the Workflow Engine that the specified activity has been completed
 * for a particular item. This procedure can be called either to indicate a
 * completed activity with an optional result or to create and start an item.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param activity Activity Label to Complete
 * @param result Result
 * @param raise_engine_exception Raise Exception for Engine Issues
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Complete Activity
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#comact See the related online help
 */
procedure CompleteActivity(itemtype in varchar2,
                           itemkey  in varchar2,
                           activity in varchar2,
                           result   in varchar2,
                           raise_engine_exception in boolean default FALSE);

--
-- CompleteActivityInternalName (PUBLIC)
--   Identical to CompleteActivity, except that the internal name of
--   completed activity is passed instead of the activity instance label.
-- NOTES:
-- 1. There must be exactly ONE instance of this activity with NOTIFIED
--    status.
-- 2. Using this api to start a new process is not supported.
-- 3. Synchronous processes are not supported in this api.
-- 4. This should only be used if for some reason the instance label is
--    not known.  CompleteActivity should be used if the instance
--    label is known.
-- IN
--   itemtype  - A valid item type
--   itemkey   - A string generated from the application object's primary key.
--   activity  - Internal name of completed activity, in the format
--               [<parent process_name>:]<process activity_name>
--   <result>  - An optional result.
--
/*#
 * Notifies the Workflow Engine that the specified activity has been completed
 * for a particular item. This procedure requires that the activity currently
 * has a status of 'Notified'. An optional activity completion result can also
 * be passed. The result can determine what transition the process takes next.
 * This API is similar to CompleteActivity() except that this API identifies the
 * activity to be completed by the activity's internal name, while
 * CompleteActivity() identifies the activity by the activity node label name.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param activity Activity Label to Complete
 * @param result Result
 * @param raise_engine_exception Raise Exception for Engine Issues
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Complete Activity Internal Name
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#comactin See the related online help
 */
procedure CompleteActivityInternalName(
  itemtype in varchar2,
  itemkey  in varchar2,
  activity in varchar2,
  result   in varchar2,
  raise_engine_exception in boolean default FALSE);

--
-- AssignActivity (PUBLIC)
--   Assigns or re-assigns the user who will perform an activity. It may be
--   called before the activity has been enabled(transitioned to). If a user
--   is assigned to an activity that already has an outstanding notification,
--   that notification will be canceled and a new notification will be
--   generated for the new user.
-- IN
--   itemtype     - A valid item type
--   itemkey      - A string generated from the application object's primary key.
--   activity     - Activity to assign, specified in the form
--                  [<parent process_name>:]<process instance_label>
--   performer    - User who will perform this activity.
--   reassignType - DELEGATE, TRANSFER or null
--   ntfComments  - Comments while reassigning
/*#
 * Assigns or reassigns an activity to another performer. This procedure may be
 * called before the activity is transitioned to. For example, a function
 * activity earlier in the process may determine the performer of a later
 * activity. If a new user is assigned to a notification activity that already
 * has an outstanding notification, the outstanding notification is canceled and
 * a new notification is generated for the new user by calling
 * WF_Notification.Transfer.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param activity Activity Label
 * @param performer Performer Role
 * @param reassignType For Internal Use Only
 * @param ntfComments For Internal Use Only
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Assign Performer to Activity
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_assigp See the related online help
 */
procedure AssignActivity(itemtype in varchar2,
                         itemkey  in varchar2,
                         activity in varchar2,
                         performer in varchar2,
                         reassignType in varchar2 default null,
                         ntfComments in varchar2 default null);

--
-- HandleError (PUBLIC)
--   Reset the process thread to given activity and begin execution
-- again from that point.  If command is:
--     SKIP - mark the activity complete with given result and continue
--     RETRY - re-execute the activity before continuing
-- IN
--   itemtype  - A valid item type.
--   itemkey   - The item key of the process.
--   activity  - Activity to reset, specified in the form
--               [<parent process_name>:]<process instance_label>
--   command   - SKIP or RETRY.
--   <result>  - Activity result for the "SKIP" command.
--
/*#
 * Handles any process activity that has encountered an error, when
 * called from an activity in an ERROR process. You can also call this procedure
 * for any arbitrary activity in a process, to rollback part of your process to
 * that activity. The activity that you call this procedure with can have any
 * status and does not need to have been executed. The activity can also be in a
 * subprocess. If the activity node label is not unique within the process you
 * may precede the activity node label name with the internal name of its parent
 * process.
 * For example, <parent_process_internal_name>:<label_name>.
 * This procedure clears the activity specified and all activities following it
 * that have already been transitioned to by reexecuting each activity in
 * 'Cancel' mode. For an activity in the 'Error' state, there are no other
 * executed activities following it, so the procedure simply clears the errored
 * activity. Once the activities are cleared, this procedure resets any parent
 * processes of the specified activity to a status of 'Active', if they are not
 * already active. The procedure then handles the specified activity based on
 * the command you provide: SKIP or RETRY.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param activity Activity Label to Handle
 * @param command Command (SKIP or RETRY)
 * @param result Result
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Handle Error
 * @rep:compatibility S
 * @rep:businessevent oracle.apps.wf.engine.skip
 * @rep:businessevent oracle.apps.wf.engine.retry
 * @rep:ihelp FND/@eng_api#a_handp See the related online help
 */
procedure HandleError(itemtype in varchar2,
                      itemkey  in varchar2,
                      activity in varchar2,
                      command  in varchar2,
                      result   in varchar2 default '');

-- HandleErrorAll (PUBLIC)
--   Reset the process thread to the given item type and/or item key.
--   It only run in RETRY mode.
-- IN
--   itemtype  - A valid item type.
--   itemkey   - The item key of the process.
--   docommit  - True if you want a commit for every n iterations.
--               n is defined as wf_engine.commit_frequency
--
procedure HandleErrorAll(itemtype in varchar2,
                         itemkey  in varchar2 default null,
                         activity in varchar2 default null,
                         command  in varchar2 default null,
                         result   in varchar2 default '',
                         docommit in boolean  default true);

/*#
 * Returns the status and result for the root process of the specified item
 * instance. Possible values returned for the status are: ACTIVE, COMPLETE,
 * ERROR, or SUSPENDED. If the root process does not exist, then the item
 * key does not exist and will thus cause the procedure to raise an exception.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param status Status
 * @param result Result
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Item Status
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_is See the related online help
 */
procedure ItemStatus(itemtype in varchar2,
                     itemkey  in varchar2,
                     status   out NOCOPY varchar2,
                     result   out NOCOPY varchar2);

procedure ItemInfo(itemtype      in  varchar2,
                   itemkey       in  varchar2,
                   status        out NOCOPY varchar2,
                   result        out NOCOPY varchar2,
                   actid         out NOCOPY number,
                   errname       out NOCOPY varchar2,
                   errmsg        out NOCOPY varchar2,
                   errstack      out NOCOPY varchar2);


--
-- Activity_Exist_In_Process
--   ### OBSOLETE - Use FindActivity instead ###
--
function Activity_Exist_In_Process (
  p_item_type          in  varchar2,
  p_item_key           in  varchar2,
  p_activity_item_type in  varchar2 default null,
  p_activity_name      in  varchar2)
return boolean;

--
-- Activity_Exist
--   ### OBSOLETE - Use FindActivity instead ###
--
function Activity_Exist (
  p_process_item_type  in  varchar2,
  p_process_name       in  varchar2,
  p_activity_item_type in  varchar2 default null,
  p_activity_name      in  varchar2,
  active_date          in  date default sysdate,
  iteration            in  number default 0)
return boolean;

--
-- EVENT activity related constants/functions
--

-- Activity types
eng_event           varchar2(8) := 'EVENT';    -- Event activity

-- Event directions
eng_receive         varchar2(8) := 'RECEIVE'; -- Recieve incoming event
eng_raise           varchar2(8) := 'RAISE';    -- Generate new event
eng_send            varchar2(8) := 'SEND';    -- Transfer event

-- Event activity attribute names
eng_eventname       varchar2(30) := '#EVENTNAME';
eng_eventkey        varchar2(30) := '#EVENTKEY';
eng_eventmessage    varchar2(30) := '#EVENTMESSAGE';
eng_eventoutagent   varchar2(30) := '#EVENTOUTAGENT';
eng_eventtoagent    varchar2(30) := '#EVENTTOAGENT';
eng_defaultevent    varchar2(30) := '#EVENTMESSAGE2';

-- Send event activity attribute for OTA Callback
eng_block_mode      varchar2(30)  := '#BLOCK_MODE';
eng_cb_event_name   varchar2(240) := '#CB_EVENT_NAME';
eng_cb_event_key    varchar2(2000):= '#CB_EVENT_KEY';

--
-- GetItemAttrClob (PUBLIC)
--   Get display contents of item attribute as a clob
-- NOTE
--   Returns expanded content of attribute.
--   For DOCUMENT-type attributes, this will be the actual document
--   generated.  For all other types, this will be the displayed
--   value of the attribute.
--   Use GetItemAttrText to retrieve internal key.
-- IN
--   itemtype - item type
--   itemkey - item key
--   aname - item attribute name
-- RETURNS
--   Expanded content of item attribute as a clob
--
/*#
 * Returns the value of an item type attribute in a process as a character large
 * object (CLOB).
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param aname Attribute Name
 * @return Item Attribute Value as CLOB
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Get Item Attribute Value as CLOB
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_giac See the related online help
 */
function GetItemAttrClob(
  itemtype in varchar2,
  itemkey in varchar2,
  aname in varchar2)
return clob;

--
-- GetActivityAttrClob (PUBLIC)
--   Get display contents of activity attribute as a clob
-- NOTE
--   Returns expanded content of attribute.
--   For DOCUMENT-type attributes, this will be the actual document
--   generated.  For all other types, this will be the displayed
--   value of the attribute.
--   Use GetActivityAttrText to retrieve internal key.
-- IN
--   itemtype - item type
--   itemkey - item key
--   aname - activity attribute name
-- RETURNS
--   Expanded content of activity attribute as a clob
--
/*#
 * Returns the value of an activity attribute in a process as a character large
 * object (CLOB).
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param actid Activity ID
 * @param aname Attribute Name
 * @return Activity Attribute Value as CLOB
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Get Activity Attribute Value as CLOB
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_getaac See the related online help
 */
function GetActivityAttrClob(
  itemtype in varchar2,
  itemkey in varchar2,
  actid in number,
  aname in varchar2)
return clob;

--
-- SetItemAttrEvent
--   Set event-type item attribute
-- IN
--   itemtype - process item type
--   itemkey - process item key
--   name - attribute name
--   event - attribute value
--
procedure SetItemAttrEvent(
  itemtype in varchar2,
  itemkey in varchar2,
  name in varchar2,
  event in wf_event_t);

--
-- GetItemAttrEvent
--   Get event-type item attribute
-- IN
--   itemtype - process item type
--   itemkey - process item key
--   name - attribute name
-- RETURNS
--   Attribute value
--
function GetItemAttrEvent(
  itemtype in varchar2,
  itemkey in varchar2,
  name in varchar2)
return wf_event_t;

--
-- GetActivityAttrEvent
--   Get event-type activity attribute
-- IN
--   itemtype - process item type
--   itemkey - process item key
--   actid - current activity id
--   name - attribute name
-- RETURNS
--   Attribute value
--
function GetActivityAttrEvent(
  itemtype in varchar2,
  itemkey in varchar2,
  actid in number,
  name in varchar2)
return wf_event_t;

--
-- Event
--   Signal event to workflow process
-- IN
--   itemtype - Item type of process
--   itemkey - Item key of process
--   process_name - Process to start (only if process not already running)
--   event_message - Event message payload
--
/*#
 * Receives an event from the Business Event System into a workflow
 * process. If the specified item key already exists, the event is received into
 * that item. If the item key does not already exist, but the specified process
 * includes an eligible Receive event activity marked as a Start activity, the
 * Workflow Engine creates a new item running that process. Within the workflow
 * process that receives the event, the procedure searches for eligible Receive
 * event activities. An activity is only eligible to receive an event if its
 * event filter is either blank, set to an event group of which that event is a
 * member, or set to that particular event. Additionally, the activity must have
 * an appropriate status.
 * @param itemtype Item Type
 * @param itemkey Item Key
 * @param process_name Process Name
 * @param event_message Event Message
 * @rep:scope public
 * @rep:lifecycle active
 * @rep:displayname Event
 * @rep:compatibility S
 * @rep:ihelp FND/@eng_api#a_engevent See the related online help
 */
procedure Event(
  itemtype in varchar2,
  itemkey in varchar2,
  process_name in varchar2 default null,
  event_message in wf_event_t);

--
-- Event2
--   Signal event to workflow process
-- IN
--   event_message - Event message payload
--
procedure Event2(
  event_message in wf_event_t);

--
-- AddToItemAttrNumber
--   Increments (or decrements) an numeric item attribute and returns the
--   new value.  If the item attribute does not exist, it returns null.
-- IN
--   p_itemtype - process item type
--   p_itemkey - process item key
--   p_aname - Item Attribute Name
--   p_name - attribute name
--   p_addend - Numeric value to be added to the item attribute.
--
-- RETURNS
--   Attribute value (NUMBER) or NULL if attribute does not exist.
--
function AddToItemAttrNumber(
  p_itemtype in varchar2,
  p_itemkey in varchar2,
  p_aname in varchar2,
  p_addend in number)
return number;

-- Bug 5903106
-- HandleErrorConcurrent
--   Concurrent Program API to handle any process activity that has
--   encountered an error. This Concurrent Program API is a wrapper
--   to HandleError and HandleErrorAll based on the parameter values
--   supplied.
-- IN
--   p_errbuf
--   p_retcode
--   p_itemtype   - Workflow Itemtype
--   p_itemkey    - Itemkey of the process
--   p_activity   - Workflow process activity label
--   p_start_date - Errored On or After date
--   p_end_date   - Errored On or Before date
--   p_max_retry  - Maximum retries allowed on an activity
--   p_docommit   - True if you want a commit for every n iterations.
--                  n is defined as wf_engine.commit_frequency
--
procedure HandleErrorConcurrent(p_errbuf    out nocopy varchar2,
                                p_retcode   out nocopy varchar2,
                                p_itemtype  in  varchar2,
                                p_itemkey   in  varchar2 default null,
                                p_process   in  varchar2 default null,
                                p_activity  in  varchar2 default null,
                                p_start_date in varchar2 default null,
                                p_end_date  in  varchar2 default null,
                                p_max_retry in  varchar2 default null,
                                p_docommit  in  varchar2 default null);
-- bug 6161171
procedure AbortProcess2(itemtype    in varchar2,
                        itemkey     in varchar2,
                        process     in varchar2       default '',
                        result      in varchar2       default wf_engine.eng_force,
                        verify_lock in binary_integer default 0,
                        cascade     in binary_integer default 0);


END WF_ENGINE;
/

create or replace package ame_util AUTHID CURRENT_USER as
/* $Header: ameoutil.pkh 120.5 2012/01/19 06:12:34 rpahune ship $ */
  /* user-defined data types */
  /* The id and string types are for various record definitions and local-variable declarations. */
  charValue varchar2(1);
  parameterValue varchar2(320);
  boilerplateValue varchar2(80);
  longBoilerplateValue varchar2(300);
  attributeValue varchar2(100);
  stringValue varchar2(100);
  longStringValue varchar2(500);
  longestStringValue varchar2(4000);
  subtype charType is charValue%type;
  subtype parameterType is parameterValue%type;
  subtype boilerplateType is boilerplateValue%type;
  subtype attributeValueType is attributeValue%type;
  subtype stringType is stringValue%type;
  subtype longStringType is longStringValue%type;
  subtype longestStringType is longestStringValue%type;
  /*
    The longBoilerplateType and longBoilerplateList datatypes are for
    boilerplate strings that are too long to be stored as AK attributes.  The
    English-language values of such strings should not exceed 100 bytes
    (characters), so that their translates values will not exceed
    300 bytes.  Always fetch a long-boilerplate string into a local variable of
    type longBoilerplateType or longBoilerplateList.
  */
  subtype longBoilerplateType is longBoilerplateValue%type;
  type longBoilerplateList is table of longBoilerplateType index by binary_integer;
  parameterTypeLength constant integer := 320;
  attributeValueTypeLength constant integer := 100;
  stringTypeLength constant integer := 100;
  longStringTypeLength constant integer := 500;
  longestStringTypeLength constant integer := 4000;
  type idStringRecord is record(
    id integer,
    string stringType);
  type attributeValueList is table of attributeValueType index by binary_integer;
  type charList is table of charType index by binary_integer;
  type dateList is table of date index by binary_integer;
  type idList is table of integer index by binary_integer;
  type idStringTable is table of idStringRecord index by binary_integer;
  type numberList is table of number index by binary_integer;
  type boilerplateList is table of boilerplateType index by binary_integer;
  type stringList is table of stringType index by binary_integer;
  type longStringList is table of longStringType index by binary_integer;
  type longestStringList is table of longestStringType index by binary_integer;
  /* engine and API types */
  type approvalGroupMemberRecord is record(
    group_id integer,
    name varchar2(320),
    orig_system varchar2(48),
    orig_system_id integer);
  type approvalProcessRecord is record(
    line_item_id integer,
    rule_id integer,
    rule_type integer,
    action_type_id integer,
    parameter parameterType,
    priority integer,
    approver_category varchar2(1));
  type approverRecord is record(
    user_id fnd_user.user_id%type,
    person_id per_all_people_f.person_id%type,
    first_name per_all_people_f.first_name%type,
    last_name per_all_people_f.last_name%type,
    api_insertion varchar2(1),
    authority varchar2(1),
    approval_status varchar2(50),
    approval_type_id integer,
    group_or_chain_id integer,
    occurrence integer,
    source varchar2(500));
  type approverRecord2 is record(
    name varchar2(320),
    orig_system varchar2(30),
    orig_system_id number,
    display_name varchar2(360),
    approver_category varchar2(1),
    api_insertion varchar2(1),
    authority varchar2(1),
    approval_status varchar2(50),
    action_type_id integer,
    group_or_chain_id integer,
    occurrence integer,
    source varchar2(500),
    item_class ame_item_classes.name%type,
    item_id ame_temp_old_approver_lists.item_id%type,
    item_class_order_number integer,
    item_order_number integer,
    sub_list_order_number integer,
    action_type_order_number integer,
    group_or_chain_order_number integer,
    member_order_number integer,
    approver_order_number integer);
  type attributeValueRecord is record(
    attribute_value_1 attributeValueType,
    attribute_value_2 attributeValueType,
    attribute_value_3 attributeValueType);
  type handlerTransStateRecord is record(
    handler_name stringType,
    state stringType);
  type insertionRecord is record(
    order_type varchar2(50),
    parameter ame_temp_insertions.parameter%type,
    api_insertion varchar2(1),
    authority varchar2(1),
    description ame_temp_insertions.description%type);
  type insertionRecord2 is record(
    item_class ame_item_classes.name%type,
    item_id ame_temp_old_approver_lists.item_id%type,
    action_type_id integer,
    group_or_chain_id integer,
    order_type varchar2(50),
    parameter ame_temp_insertions.parameter%type,
    api_insertion varchar2(1),
    authority varchar2(1),
    description ame_temp_insertions.description%type);
/*
AME_STRIPING
  type lineItemStripeRuleRecord is record(
    line_item_id integer,
    stripe_set_id integer,
    rule_id integer,
    rule_type integer,
    action_type_id integer,
    parameter parameterType,
    priority integer);
*/
  type orderRecord is record(
    order_type varchar2(50),
    parameter ame_temp_insertions.parameter%type,
    description varchar2(200));
  type workflowLogRecord is record(
    package_name varchar2(50),
    routine_name varchar2(50),
    log_id integer,
    transaction_id varchar2(50),
    exception_number integer,
    exception_string longestStringType);
  /* Record to represent a engine approver tree node */
  type approverTreeRecord is record(
    parent_index   integer,
    child_index    integer,
    sibling_index  integer,
    approver_index integer,
    tree_level     integer,
    tree_level_id  varchar2(320),
    order_number   integer,
    min_order      integer,
    max_order      integer,
    status         integer,
    is_suspended    varchar2(1));
  /* Data Type to store the engine approver tree */
  type approversTreeTable is table of approverTreeRecord index by binary_integer;
  type approvalGroupMembersTable is table of approvalGroupMemberRecord index by binary_integer;
  type approvalProcessTable is table of approvalProcessRecord index by binary_integer;
  type approversTable is table of approverRecord index by binary_integer;
  type approversTable2 is table of approverRecord2 index by binary_integer;
  type sVAttributeValuesTable is table of attributeValueType index by binary_integer;
  type attributeValuesTable is table of attributeValueRecord index by binary_integer;
  type exceptionLogTable is table of ame_exceptions_log%rowtype index by binary_integer;
  type handlerTransStateTable is table of handlerTransStateRecord index by binary_integer;
  type insertionsTable is table of insertionRecord index by binary_integer;
  type insertionsTable2 is table of insertionRecord2 index by binary_integer;

/*
AME_STRIPING
  type lineItemStripeRuleTable is table of lineItemStripeRuleRecord index by binary_integer;
*/
  type ordersTable is table of orderRecord index by binary_integer;
  type parametersTable is table of parameterType index by binary_integer;
  type workflowLogTable is table of workflowLogRecord index by binary_integer;
  /* misc. types */
  type queryCursor is ref cursor;
  /* Constants used by the engine to process the approver tree */
  noChildIndex     constant integer := -1;
  noParentIndex    constant integer := -1;
  noSiblingIndex   constant integer := -1;
  noApproverIndex  constant integer := -1;
  invalidTreeIndex constant integer := -1;
  unknownStatus    constant integer := -1;
  startedStatus    constant integer :=  3;
  notStartedStatus constant integer :=  2;
  completedStatus  constant integer :=  1;
  firstAmongEquals constant boolean := true;
  lastAmongEquals  constant boolean := false;
  minimumApproverOrderNumber constant integer := 1;
  /* misc. constants */
  ameShortName constant varchar2(3) := 'AME';
  defaultDateFormatModel constant varchar2(50) := 'DD-MM-YYYY';
  oneSecond constant number := 1/86400;
  mandAttActionTypeId constant integer := -1;
  seededDataCreatedById constant integer := 1;
  yes constant varchar(3) := 'yes';
  no constant varchar2(2) := 'no';
  testTrans constant varchar2(10) := 'testTrans';
  realTrans constant varchar2(10) := 'realTrans';
  /* special wf_roles.name values */
  invalidApproverWfRolesName constant varchar2(50) := 'AME_INVALID_APPROVER';
  /* ICX constants */
  developerResponsibility constant integer := 5;
  appAdminResponsibility constant integer := 4;
  genBusResponsibility constant integer := 3;
  limBusResponsibility constant integer := 2;
  readOnlyResponsibility constant integer := 1;
  noResponsibility constant integer := 0;
  devRespKey constant varchar2(50) := 'AMEDEVELOPER';
  appAdminRespKey constant varchar2(50) := 'AMEAPPADM';
  genBusUserRespKey constant varchar2(50) := 'AMEGENUSER';
  limBusUserRespKey constant varchar2(50) := 'AMELIMUSER';
  readOnlyUserRespKey constant varchar2(50) := 'AMEROUSER';
  webFunction constant varchar2(17) := 'AME_WEB_APPROVALS';
  attributeCode constant varchar2(26) := 'AME_INTERNAL_TRANS_TYPE_ID';
  /* per fnd app id */
  perFndAppId constant integer := 800;
  /* generalized-approver-type (GAT) constants */
  /* GAT originating-system constants */
  fndRespOrigSystem constant varchar2(8) := 'FND_RESP';
  fndUserOrigSystem constant varchar2(7) := 'FND_USR';
  perOrigSystem constant varchar2(3) := 'PER';
  posOrigSystem constant varchar2(3) := 'POS';
  /* GAT lookup types */
  origSystemLookupType constant fnd_lookups.lookup_type%type := 'FND_WF_ORIG_SYSTEMS';
  /* GAT approver categories */
  approvalApproverCategory constant varchar2(1) := 'A';
  fyiApproverCategory constant varchar2(1) := 'F';
  /* GAT other constants */
  anyApproverType constant integer := -1;
  approverWfRolesName constant varchar2(50) := 'wf_roles_name';
  /* developer-key constants */
  devKeyPlaceHolder constant varchar2(9) := 'CHANGE_ME';
  seededKeyPrefix constant varchar2(4) := 'SEED';
  /* chain-of-authority ordering modes */
  parallelChainsMode constant varchar2(1) := 'P';
  serialChainsMode constant varchar2(1) := 'S';
  /* The following values are the only allowed values for
     ame_approval_group_config.voting_regime.  All of the values other
     than orderNumberVoting are also allowed for
     ame_action_type_config.voting_regime. */
  consensusVoting constant varchar2(1) := 'C';
  firstApproverVoting constant varchar2(1) := 'F';
  orderNumberVoting constant varchar2(1) := 'O';
  serializedVoting constant varchar2(1) := 'S';
  /*
    None of the empty[whatever] variables below should ever be overwritten. They
    are only to be used as default arguments where empty defaults are required.
  */
  emptyApproverRecord approverRecord;
  emptyApproverRecord2 approverRecord2;
  emptyApproversTable approversTable;
  emptyApproversTable2 approversTable2;
  emptyDateList dateList;
  emptyDbmsSqlVarchar2Table dbms_sql.varchar2_table;
  emptyExceptionLogTable exceptionLogTable;
  emptyIdList idList;
  emptyIdStringTable idStringTable;
  emptyInsertionRecord insertionRecord;
  emptyInsertionRecord2 insertionRecord2;
  emptyInsertionsTable insertionsTable;
  emptyInsertionsTable2 insertionsTable2;
  emptyParametersTable parametersTable;
  emptyOrderRecord orderRecord;
  emptyOrdersTable ordersTable;
  emptyOwaUtilIdentArr owa_util.ident_arr;
  emptyCharList charList;
  emptyAttributeValueList attributeValueList;
  emptyStringList stringList;
  emptyLongStringList longStringList;
  emptyLongestStringList longestStringList;
  emptyWorkflowLogTable workflowLogTable;
  /* approver types */
  approverPersonId constant varchar2(50) := 'person_id';
  approverUserId constant varchar2(50) := 'user_id';
  approverOamGroupId constant varchar2(50) := 'OAM_group_id';
  /*
    The following constants are the only allowed values in an approverRecord2's
    api_insertion field.  They have the following meanings.  (1) An
    apiAuthorityInsertion is an insertion into the chain of authority requiring
    the chain to jump at the insertion.  (2) An apiInsertion is an "ad hoc"
    insertion.  If it occurs in the chain of authority, it does not require the
    chain to jump at the insertion.  The chain skips over the ad-hoc insertion.
    (3) An oamGenerated approver is not an insertion, but is generated by the
    rules in OAM applying to the transaction.
  */
  apiAuthorityInsertion constant varchar2(1) := 'A';
  apiInsertion constant varchar2(1) := 'Y';
  oamGenerated constant varchar2(1) := 'N';
  /*
    The following constants are the only allowed values in an approverRecord2's
    authority field.  Note that the constants are backwards compatible with the
    original Y/N values, and are also ordered so that one can select approvers
    in the proper order out of ame_temp_approval_processes.
  */
  preApprover constant varchar2(1) := 'N';
  authorityApprover constant varchar2(1) := 'Y';
  postApprover constant varchar2(1) := 'Z';
  /*
    The following constants are the only allowed values in an approverRecord2's
    approval_status fields.  Note that there is no 'D' on the end of 'APPROVE'
    in the value of approvedStatus; this is intentional.
  */
  approveAndForwardStatus constant varchar2(20) := 'APPROVE AND FORWARD';
  approvedStatus constant varchar2(20) := 'APPROVE';
  beatByFirstResponderStatus constant varchar2(50) := 'BEAT BY FIRST RESPONDER';
  clearExceptionsStatus constant varchar2(20) := 'CLEAR EXCEPTIONS';
  exceptionStatus constant varchar2(20) := 'EXCEPTION';
  forwardStatus constant varchar2(20) := 'FORWARD';
  noResponseStatus constant varchar2(20) := 'NO RESPONSE';
  notifiedStatus constant varchar2(20) := 'NOTIFIED';
  nullStatus constant varchar2(1) := null;
  rejectStatus constant varchar2(20) := 'REJECT';
  repeatedStatus constant varchar2(20) := 'REPEATED';
  suppressedStatus constant varchar2(20) := 'SUPPRESSED';
  /* New status added for asynchronous parallel approver functionality*/
  notifiedByRepeatedStatus constant varchar2(20) := 'NOTIFIEDBYREPEATED';
  approvedByRepeatedStatus constant varchar2(20) := 'APPROVEDBYREPEATED';
  rejectedByRepeatedStatus constant varchar2(20) := 'REJECTEDBYREPEATED';
  /*
    The following values are components of the only allowed values of the source field
    of an approverRecord2, when the record's api_insertion value is not ame_util.oamGenerated.
  */
  approveAndForwardInsertion constant varchar2(20) := 'APPROVE_AND_FORWARD';
  forwardInsertion constant varchar2(20) := 'FORWARD';
  specialForwardInsertion constant varchar2(20) := 'SPECIAL FORWARD';
  otherInsertion constant varchar2(20) := 'OTHER';
  surrogateInsertion constant varchar2(20) := 'SURROGATE';
  /* The following value is prepended to the source field of a deleted approver. */
  apiSuppression constant varchar2(20) := 'SUPPRESSED';
  /*
    The following constants are the only allowed values in the fields of the
    forwardingBehavior configuration variable:
  */
  forwardeeOnly constant varchar2(20) := 'FORWARDEE_ONLY';
  forwarderAndForwardee constant varchar2(20) := 'FORWARDER_FORWARDEE';
  ignoreForwarding constant varchar2(20) := 'IGNORE';
  remand constant varchar2(20) := 'REMAND';
  repeatForwarder constant varchar2(20) := 'REPEAT_FORWARDER';
  skipForwarder constant varchar2(20) := 'SKIP_FORWARDER';
  /*
    The following constants are the only allowed values of the repeatedApprovers
    configuration variable:
  */
  oncePerTransaction constant varchar2(50) := 'ONCE_PER_TRANSACTION';
  oncePerItemClass constant varchar2(50) := 'ONCE_PER_ITEM_CLASS';
  oncePerItem constant varchar2(50) := 'ONCE_PER_ITEM';
  oncePerSublist  constant varchar2(50) := 'ONCE_PER_SUBLIST';
  oncePerActionType constant varchar2(50) := 'ONCE_PER_ACTION_TYPE';
  oncePerGroupOrChain constant varchar2(50) := 'ONCE_PER_GROUP_OR_CHAIN';
  eachOccurrence constant varchar2(50) := 'EACH_OCCURRENCE';
  /*
    The following constants are the only allowed values of the REJECTION_RESPONSE
    mandatory attribute.
  */
  continueAllOtherItems constant varchar2(50) := 'CONTINUE_ALL_OTHER_ITEMS';
  continueOtherSubItems constant varchar2(50) := 'CONTINUE_OTHER_SUBORDINATE_ITEMS';
  stopAllItems constant varchar2(50) := 'STOP_ALL_ITEMS';
  /*
    The ame_engine.getForwardingBehavior function requires a forwarder type
    and a forwardee type.  The values below are allowed.  When the forwarder
    is a chainOfAuthorityForwarder, alreadyInListForwardee refers to an
    approver who is already in the approver list, but not in the same chain
    of authority as the forwarder.  (When the forwarder is an adHocForwarder,
    alreadyInListForwardee is the only allowed value.)
  */
  chainOfAuthorityForwarder constant varchar2(50) := 'CHAIN_FORWARDER';
  adHocForwarder constant varchar2(50) := 'AD_HOC_FORWARDER';
  previousSameChainForwardee constant varchar2(50) := 'SAME_CHAIN_PREVIOUS_FORWARDEE';
  subordSameHierarchyForwardee constant varchar2(50) := 'SUBORDINATE_SAME_HIERARCHY';
  alreadyInListForwardee constant varchar2(50) := 'ALREADY_IN_LIST';
  /*
  The API inserts the following constants in the ame_approvals_history table.  These
  values should never appear in an approverRecord.
  */
  /* pseudo-null constants for adhoc insertions */
  nullInsertionActionTypeId constant integer := -1;
  nullInsertionGroupOrChainId constant integer := -1;
  /* pseudo-null constants for adhoc insertions. For backward compatibility only. These 2 constants
     should not be used in the AME code at all. */
  adHocInsertionApprovalTypeId constant integer := -1;
  adHocInsertionGroupOrChainId constant integer := -1;
  /* pseudo-null constants for ame_approvals_history entries */
  nullHistoryActionTypeId constant integer := 0.5;
  nullHistoryGroupOrChainId constant integer := 0.5;
  nullHistoryOccurrence constant integer := 0.5;
  nullHistoryStatus constant varchar2(20) := 'NULL';
  nullHistorySource constant varchar2(20) := 'NULL';
  /*
    The following constants are used by the API to identify types of order
    relations for ad-hoc insertions.  They are the only valid values in the
    orderRecord record's order_type field.
  */
  absoluteOrder constant varchar2(50) := 'absolute order';
  absoluteOrderDescription constant varchar2(200) :=
    'Regardless of changes in the approver list''s membership, give the new approver ' ||
    'the following order number:  ';
  afterApprover constant varchar2(50) := 'after approver';
  afterApproverDescription constant varchar2(200) :=
    'Always put the new approver right after the following approver:  ';
  beforeApprover constant varchar2(50) := 'before approver';
  beforeApproverDescription constant varchar2(200) :=
    'Always put the new approver right before the following approver:  ';
  firstAuthority constant varchar2(50) := 'first authority';
  firstAuthorityDescription constant varchar2(200) :=
    'Start all chains of authority at the new approver.';
  firstAuthorityParameter constant varchar2(100) := 'first authority';
  firstPostApprover constant varchar2(50) := 'first post-approver';
  firstPostApproverDescription constant varchar2(200) := 'Make the approver the first post-approver.';
  firstPostParameter constant varchar2(50) := 'first_post_approver';
  firstPreApprover constant varchar2(50) := 'first pre-approver';
  firstPreApproverDescription constant varchar2(200) := 'Make the approver the first pre-approver.';
  firstPreParameter constant varchar2(50) := 'first_pre_approver';
  lastPostApprover constant varchar2(50) := 'last post-approver';
  lastPostApproverDescription constant varchar2(200) := 'Make the approver the last post-approver.';
  lastPostParameter constant varchar2(50) := 'last_post_approver';
  lastPreApprover constant varchar2(50) := 'last pre-approver';
  lastPreApproverDescription constant varchar2(200) := 'Make the approver the last pre-approver.';
  lastPreParameter constant varchar2(50) := 'last_pre_approver';
  /*
    The following constants are the only placeholders that may appear in the
    query strings defining an attribute for a given application, i.e. in
    the ame_attribute_usages.query_string column.
  */
  transactionIdPlaceholder constant varchar2(50) := ':transactionId';
  /* ame_attributes.attribute_type allowed values */
  numberAttributeType constant varchar2(20) := 'number';
  stringAttributeType constant varchar2(20) := 'string';
  dateAttributeType constant varchar2(20) := 'date';
  booleanAttributeType constant varchar2(20) := 'boolean';
  currencyAttributeType constant varchar2(20) := 'currency';
  /* boolean attribute allowed values */
  booleanAttributeTrue constant varchar2(10) := 'true';
  booleanAttributeFalse constant varchar2(10) := 'false';
  /* rule type labels */
  ruleTypeLabel0 constant varchar2(50) := 'combination';
  ruleTypeLabel1 constant varchar2(50) := 'list-creation rule';
  ruleTypeLabel2 constant varchar2(50) := 'list-creation exception';
  ruleTypeLabel3 constant varchar2(50) := 'list-modification rule';
  ruleTypeLabel4 constant varchar2(50) := 'substitution';
  ruleTypeLabel5 constant varchar2(50) := 'pre-list approval-group rule';
  ruleTypeLabel6 constant varchar2(50) := 'post-list approval-group rule';
  ruleTypeLabel7 constant varchar2(50) := 'production';
  /* ame_rules.rule_type allowed values */
  combinationRuleType constant integer := 0;
  authorityRuleType constant number := 1;
  exceptionRuleType constant number := 2;
  listModRuleType constant number := 3;
  substitutionRuleType constant number := 4;
  preListGroupRuleType constant number := 5;
  postListGroupRuleType constant number := 6;
  productionRuleType constant integer := 7;
  /* ame_conditions.condition_type allowed values */
  ordinaryConditionType constant varchar2(20) := 'auth';
  exceptionConditionType constant varchar2(20) := 'pre';
  listModConditionType constant varchar2(20) := 'post';
  /* dynamic-action-description bind variables */
  actionParameterOne constant varchar2(20) := ':parameterOne';
  actionParameterTwo constant varchar2(20) := ':parameterTwo';
  /* constants for list-modification conditions */
  anyApprover constant varchar2(50) := 'any_approver';
  finalApprover constant varchar2(50) := 'final_approver';
  /* The following 2 constants only to be used for backward comp. */
  dynamicPostApprover constant varchar2(50) := 'dynamic post-approver';
  dynamicPreApprover constant varchar2(50) := 'dynamic pre-approver';
  /* pseudo-boolean constants */
  booleanTrue constant varchar2(1) := 'Y';
  booleanFalse constant varchar2(1) := 'N';
  /*
    ame_attribute_usages.user_editable possible values:
    (1) booleanTrue:  the user can edit the query string (usage), but has not
    (2) booleanFalse:  the user cannot edit the query string
    (3) userEdited:  the user has edited the query string.
    If the value is booleanTrue or booleanFalse, it's ok to overwrite the usage when
    installing a patch.  If the value is userEdited, the patch should not overwrite
    the query string.
  */
  userEdited constant varchar2(1) := 'Z';
  /* mandatory-attribute names */
  allowAutoApprovalAttribute constant varchar2(50) := 'ALLOW_REQUESTOR_APPROVAL';
  allowDeletingOamApprovers constant varchar2(50) := 'ALLOW_DELETING_RULE_GENERATED_APPROVERS';
  atLeastOneRuleAttribute constant varchar2(50) := 'AT_LEAST_ONE_RULE_MUST_APPLY';
  transactionDateAttribute constant varchar2(50) := 'TRANSACTION_DATE';
  transactionRequestorAttribute constant varchar2(50) := 'TRANSACTION_REQUESTOR_PERSON_ID';
  transactionReqUserAttribute constant varchar2(50) := 'TRANSACTION_REQUESTOR_USER_ID';
  transactionOrgAttribute constant varchar2(50) := 'TRANSACTION_ORG_ID';
  transactionGroupAttribute constant varchar2(50) := 'TRANSACTION_GROUP_ID';
  transactionSetOfBooksAttribute constant varchar2(50) := 'TRANSACTION_SET_OF_BOOKS_ID';
  effectiveRuleDateAttribute constant varchar2(50) := 'EFFECTIVE_RULE_DATE';
  useWorkflowAttribute constant varchar2(50) := 'USE_WORKFLOW';
  workflowItemKeyAttribute constant varchar2(50) := 'WORKFLOW_ITEM_KEY';
  workflowItemTypeAttribute constant varchar2(50) := 'WORKFLOW_ITEM_TYPE';
  restrictiveLIEvalAttribute constant varchar2(50) := 'USE_RESTRICTIVE_LINE_ITEM_EVALUATION';
  evalPrioritiesPerLIAttribute constant varchar2(50) := 'EVALUATE_PRIORITIES_PER_LINE_ITEM';
  restrictiveItemEvalAttribute constant varchar2(50) := 'USE_RESTRICTIVE_ITEM_EVALUATION';
  evalPrioritiesPerItemAttribute constant varchar2(50) := 'EVALUATE_PRIORITIES_PER_ITEM';
  rejectionResponseAttribute constant varchar2(50) := 'REJECTION_RESPONSE';
  repeatSubstitutionsAttribute constant varchar2(50) := 'REPEAT_SUBSTITUTIONS';
  /* attribute names referenced by job-level handlers */
  jobLevelStartingPointAttribute constant varchar2(50) := 'JOB_LEVEL_NON_DEFAULT_STARTING_POINT_PERSON_ID';
  firstStartingPointAttribute constant varchar2(50) := 'FIRST_STARTING_POINT_PERSON_ID';
  secondStartingPointAttribute constant varchar2(50) := 'SECOND_STARTING_POINT_PERSON_ID';
  includeAllApproversAttribute constant varchar2(50) := 'INCLUDE_ALL_JOB_LEVEL_APPROVERS';
  lineItemStartingPointAttribute constant varchar2(50) := 'LINE_ITEM_STARTING_POINT_PERSON_ID';
  /* attribute names referenced by position handler, and allowed values */
  positionSortMethodAttribute constant varchar2(50) := 'POSITION_APPROVER_SORT_METHOD';
  orderedPositionSort constant varchar2(50) := 'ordered';
  randomPositionSort constant varchar2(50) := 'random';
  simplePosStartPointAttribute constant varchar2(50) := 'SIMPLE_POS_NON_DEFAULT_STARTING_POINT_PERSON_ID';
  /* attribute names referenced by position handler */
  nonDefStartingPointPosAttr constant varchar2(50) := 'NON_DEFAULT_STARTING_POINT_POSITION_ID';
  nonDefPosStructureAttr constant varchar2(50) := 'NON_DEFAULT_POSITION_STRUCTURE_ID';
  transactionReqPositionAttr constant varchar2(50) := 'TRANSACTION_REQUESTOR_POSITION_ID';
  topPositionIdAttribute constant varchar2(50) := 'TOP_POSITION_ID';
  /* attribute names referenced by approval-group handers */
  allowEmptyGroupAttribute constant varchar2(50) := 'ALLOW_EMPTY_APPROVAL_GROUPS';
  /* attribute names referenced by supervisory handler */
  supStartingPointAttribute constant varchar2(50) := 'SUPERVISORY_NON_DEFAULT_STARTING_POINT_PERSON_ID';
  topSupPersonIdAttribute constant varchar2(50) := 'TOP_SUPERVISOR_PERSON_ID';
  /* referenced handler names */
  absoluteJobLevelHandlerName constant varchar2(50) := 'ame_absolute_job_level_handler';
  simplePositionHandlerName constant varchar2(50) := 'ame_simple_position_handler';
  /* referenced approval-type names */
  preApprovalTypeName constant varchar2(50) := 'pre-chain-of-authority approvals';
  absoluteJobLevelTypeName constant varchar2(50) := 'absolute job level';
  relativeJobLevelTypeName constant varchar2(50) := 'relative job level';
  supervisoryLevelTypeName constant varchar2(50) := 'supervisory level';
  positionTypeName constant varchar2(50) := 'hr position';
  positionLevelTypeName constant varchar2(50) := 'hr position level';
  managerFinalApproverTypeName constant varchar2(50) := 'manager then final approver';
  finalApproverOnlyTypeName constant varchar2(50):= 'final approver only';
  lineItemJobLevelTypeName constant varchar2(50) := 'line-item job-level chains of authority';
  dualChainsAuthorityTypeName constant varchar2(50) := 'dual chains of authority';
  groupChainApprovalTypeName constant varchar2(50) := 'approval-group chain of authority';
  nonFinalAuthority constant varchar2(50) := 'nonfinal authority';
  finalAuthorityTypeName constant varchar2(50) := 'final authority';
  substitutionTypeName constant varchar2(50) := 'substitution';
  postApprovalTypeName constant varchar2(50) := 'post-chain-of-authority approvals';
  productionActionTypeName constant varchar2(50) := 'production rule';
  /* referenced item-class names */
  costCenterItemClassName constant varchar2(100) := 'cost center';
  headerItemClassName constant varchar2(100) := 'header';
  lineItemItemClassName constant varchar2(100) := 'line item';
  /* configuration-variable names */
  adminApproverConfigVar constant varchar2(50) := 'adminApprover';
  purgeFrequencyConfigVar constant varchar2(50) := 'purgeFrequency';
  useWorkflowConfigVar constant varchar2(50) := 'useWorkflow';
  helpPathConfigVar constant varchar2(50) := 'helpPath';
  htmlPathConfigVar constant varchar2(50) := 'htmlPath';
  imagePathConfigVar constant varchar2(50) := 'imagePath';
  portalUrlConfigVar constant varchar2(50) := 'portalUrl';
  distEnvConfigVar constant varchar2(50) := 'distributedEnvironment';
  curConvWindowConfigVar constant varchar2(50) := 'currencyConversionWindow';
  forwardingConfigVar constant varchar2(50) := 'forwardingBehaviors';
  repeatedApproverConfigVar constant varchar2(50) := 'repeatedApprovers';
  allowAllApproverTypesConfigVar constant varchar2(30) := 'allowAllApproverTypes';
  allowFyiNotificationsConfigVar constant varchar2(30) := 'allowFyiNotifications';
  rulePriorityModesConfigVar constant varchar2(50) := 'rulePriorityModes';
  productionConfigVar constant varchar2(50) := 'productionFunctionality';
  allowAllICRulesConfigVar constant varchar2(50) := 'allowAllItemClassRules';
  /* productionFunctionality allowed values */
  noProductions constant varchar2(50) := 'none';
  perApproverProductions constant varchar2(50) := 'approver';
  perTransactionProductions constant varchar2(50) := 'transaction';
  allProductions constant varchar2(50) := 'all';
  /* productionFunctionality exception */
  productionException exception;
  /* chain-of-authority ordering modes */
  sequentialChainsMode constant varchar2(1) := 'S';
  /*
    allowedValues for headerListParallelizationMode configuration variable
  */
  headerAfterItems constant varchar2(1) := 'A';
  headerBeforeItems constant varchar2(1) := 'B';
  headerWithFirstItems constant varchar2(1) := 'W';
  /*
    The following constants are the only allowed values for the
    ame_item_class_usages.item_class_sublist_mode column.
  */
  parallelSublists constant varchar2(1) := 'P';
  preFirst constant varchar2(1) := 'R';
  preAndAuthorityFirst constant varchar2(1) := 'A';
  serialSublists constant varchar2(1) := 'S';
  /*
    The following constants are the only allowed values
    for the ame_item_class_usages.item_class_par_mode
    column.
  */
  parallelItems constant varchar2(1) := 'P';
  serialItems constant varchar2(1) := 'S';
/*
AME_STRIPING
  useRuleStripingConfigVar constant varchar2(50) := 'useRuleStriping';
*/
  /* rulePriorityModes */
  disabledRulePriorityDefVal constant varchar2(100) := 'disabled:disabled:disabled:disabled:disabled:disabled:disabled:disabled';
  disabledRulePriority constant varchar2(50) := 'disabled';
  absoluteRulePriority constant varchar2(50) := 'absolute';
  relativeRulePriority constant varchar2(50) := 'relative';
  /* sourceDescriptionOut */
  /* one or more rules required the approver. */
  ruleGeneratedSource constant varchar2(50) := 'rule generated' ;
  /* Another approver forwarded to this approver, or this approver forwarded to another approver, and
  the repeatedApprover configuration variable's value required that this approver re-approve after the
  forwardee approves. */
  forwardeeSource constant varchar2(50) :=  'forwardee';
  /* An end user of the originating application inserted the approver from within the application. */
  inserteeSource constant varchar2(50) := 'insertee';
  /* Another approver was unresponsive, and the originating application so informed AME.  AME
  added this approver to serve as a surrogate for the unresponsive approver. */
  surrogateSource constant varchar2(50) :='surrogate';
  /* The calling application deleted the approver (via an API call). */
  suppressionSource constant varchar2(50) := 'suppressed by end user';
  /* versionStartDate stuff */
  objectVersionException exception;
  versionDateFormatModel constant varchar2(50) := 'YYYY:MM:DD:HH24:MI:SS';
  /* Approver-query-handler procecdures should raise this exception when they fetch more than 50 rows. */
  tooManyApproversException exception;
  /* Approver-query-handler procecdures should raise this exception when they fetch zero rows. */
  zeroApproversException exception;
  /* transaction-type cookie name */
  transactionTypeCookie constant varchar2(50) := 'AME_TRANSACTION_TYPE';
  /* admin-activity types */
  applicationAdministration constant varchar2(50) := 'applicationAdministration';
  transactionTypeAdministration constant varchar2(50) := 'transactionTypeAdministration';
  /* condition navigation paths */
  stringListForm constant varchar2(50) := 'stringListForm';
  conditionListForm constant varchar2(50) := 'conditionListForm';
  /* UI style-sheet constants (one per class in amestyle.css) */
  activeFooterItemStyle constant varchar2(30) := 'activeFooterItem';
  detailsHeadingStyle constant varchar2(30) := 'detailsHeading';
  detailsLabelStyle constant varchar2(30) := 'detailsLabel';
  editFormLinkStyle constant varchar2(30) := 'editFormLink';
  footerItemStyle constant varchar2(30) := 'footerItem';
  formOrListHeadingStyle constant varchar2(30) := 'formOrListHeading';
  listDeleteHeadingStyle constant varchar2(30) := 'listDeleteHeading';
  listSubheadingStyle constant varchar2(30) := 'listSubheading';
  navLinkStyle constant varchar2(30) := 'navLink';
  staticDescriptionStyle constant varchar2(30) := 'staticDescription';
  transTypeHeadingStyle constant varchar2(30) := 'transTypeHeading';
  twoColumnFormLabelStyle constant varchar2(30) := 'twoColumnFormLabel';
/*
AME_STRIPING
  rule-striping stuff
  stripeWildcard varchar2(50) := 'AME_*';
*/
  /* functions and procedures */
  /*
    canonNumStringToDisplayString formats canonicalNumberStringIn according to the formatting
    conventions for the currency represented by currencyCodeIn.  Otherwise, at present,
    the input string's formatting is unchanged.  We should nevertheless call this function to
    format number-strings for display so that we can easily upgrade our number displays to NLS
    conformance, when that becomes possible.
  */
  function canonNumStringToDisplayString(canonicalNumberStringIn in varchar2,
                                         currencyCodeIn in varchar2 default null) return varchar2;
  function convertCurrency(fromCurrencyCodeIn in varchar2,
                           toCurrencyCodeIn in varchar2,
                           conversionTypeIn in varchar2,
                           amountIn in number,
                           dateIn in date default sysdate,
                           applicationIdIn in integer default null) return number;
  /*
    dateStringsToString takes values YYYY, MM, and DD and transforms them to a date-string
    with the format versionDateFormatModel.  Use it in handlers for forms having the
    ame_util.twoColumnFormDateInput widget.
  */
  function dateStringsToString(yearIn in varchar2,
                               monthIn in varchar2,
                               dayIn in varchar2) return varchar2;
  function escapeSpaceChars(stringIn in varchar2) return varchar2;
  function fieldDelimiter return varchar2;
  function filterHtmlUponInput(stringIn in varchar2) return varchar2;
  function filterHtmlUponRendering(stringIn in varchar2) return varchar2;
  function getAdminName(applicationIdIn in integer default null) return varchar2;
  function getBusGroupName(busGroupIdIn in integer) return varchar2;
  /*
    getCarriageReturn returns ASCII 13, a carriage return.  Some Web browsers combine this
    character with ASCII 10, a line feed (which is what the Enter key generates), to create
    a new line in a textarea input.  The removeReturns function removes both of these
    characters, optionally replacing them by a space character, and returning the result.
    When you want to strip the returns out of a string, use removeReturns.
  */
  function getCarriageReturn return varchar2;
  function getContactAdminString(applicationIdIn in integer default null) return varchar2;
  function getColumnLength(tableNameIn        in varchar2,
                           columnNameIn       in varchar2,
                           fndApplicationIdIn in integer default 800) return integer;
  function getConfigDesc(variableNameIn in varchar2) return varchar2;
  function getConfigVar(variableNameIn in varchar2,
                        applicationIdIn in integer default null) return varchar2;
  function getCurrencyName(currencyCodeIn in varchar2) return varchar2;
/*
AME_STRIPING
  function getCurrentStripeSetId(applicationIdIn in integer) return integer;
*/
  function getCurrentUserId return integer;
  function getDayString(dateIn in date) return varchar2;
  function getHighestResponsibility return integer;
  /* getLineFeed returns ASCII character 10, which is what the return key produces. */
  function getLabel(attributeApplicationIdIn in number,
                    attributeCodeIn          in varchar2,
                    returnColonAndSpaces     in boolean default false) return varchar2;
  function getLineFeed return varchar2;
  function getLongBoilerplate(applicationShortNameIn in varchar2,
                              messageNameIn          in varchar2,
                              tokenNameOneIn        in varchar2 default null,
                              tokenValueOneIn       in varchar2 default null,
                              tokenNameTwoIn        in varchar2 default null,
                              tokenValueTwoIn       in varchar2 default null,
                              tokenNameThreeIn      in varchar2 default null,
                              tokenValueThreeIn     in varchar2 default null,
                              tokenNameFourIn       in varchar2 default null,
                              tokenValueFourIn      in varchar2 default null,
                              tokenNameFiveIn       in varchar2 default null,
                              tokenValueFiveIn      in varchar2 default null,
                              tokenNameSixIn        in varchar2 default null,
                              tokenValueSixIn       in varchar2 default null,
                              tokenNameSevenIn      in varchar2 default null,
                              tokenValueSevenIn     in varchar2 default null,
                              tokenNameEightIn      in varchar2 default null,
                              tokenValueEightIn     in varchar2 default null,
                              tokenNameNineIn       in varchar2 default null,
                              tokenValueNineIn      in varchar2 default null,
                              tokenNameTenIn        in varchar2 default null,
                              tokenValueTenIn       in varchar2 default null) return varchar2;
  function getMessage(applicationShortNameIn in varchar2,
                      messageNameIn          in varchar2,
                      tokenNameOneIn        in varchar2 default null,
                      tokenValueOneIn       in varchar2 default null,
                      tokenNameTwoIn        in varchar2 default null,
                      tokenValueTwoIn       in varchar2 default null,
                      tokenNameThreeIn      in varchar2 default null,
                      tokenValueThreeIn     in varchar2 default null,
                      tokenNameFourIn       in varchar2 default null,
                      tokenValueFourIn      in varchar2 default null,
                      tokenNameFiveIn       in varchar2 default null,
                      tokenValueFiveIn      in varchar2 default null,
                      tokenNameSixIn        in varchar2 default null,
                      tokenValueSixIn       in varchar2 default null,
                      tokenNameSevenIn      in varchar2 default null,
                      tokenValueSevenIn     in varchar2 default null,
                      tokenNameEightIn      in varchar2 default null,
                      tokenValueEightIn     in varchar2 default null,
                      tokenNameNineIn       in varchar2 default null,
                      tokenValueNineIn      in varchar2 default null,
                      tokenNameTenIn        in varchar2 default null,
                      tokenValueTenIn       in varchar2 default null) return varchar2;
  function getMonthString(dateIn in date) return varchar2;
  function getOrgName(orgIdIn in integer) return varchar2;
  function getPlsqlDadPath return varchar2;
  function getQuery(selectClauseIn in varchar2) return ame_util.queryCursor ;
  function getServerName return varchar2;
  function getServerPort return varchar2;
  function getSetOfBooksName(setOfBooksIdIn in integer) return varchar2;
  function getTransTypeCookie return integer;
/*
AME_STRIPING
  function getStripeSetCookieName(applicationIdIn in integer) return varchar2;
*/
  function getYearString(dateIn in date) return varchar2;
  function hasOrderClause(queryStringIn in varchar2) return boolean;
  function idListsMatch(idList1InOut in out nocopy idList,
                        idList2InOut in out nocopy idList,
                        sortList1In in boolean default false,
                        sortList2In in boolean default true) return boolean;
  /*
    inputNumStringToCanonNumString converts inputNumberStringIn to a number-string in
    canonical format, for storage in database tables.  It also validates the input.  If
    currencyCodeIn is non-null, the validation includes checking the proper number of
    decimal places for a currency number-string.  We should always use this function to
    convert input number-strings to storable number-strings.
  */
  function inputNumStringToCanonNumString(inputNumberStringIn in varchar2,
                                          currencyCodeIn in varchar2 default null) return varchar2;
  function isAnEvenNumber(numberIn in integer) return boolean;
  function isAnInteger(stringIn in varchar2) return boolean;
  function isANegativeInteger(stringIn in varchar2) return boolean;
  function isANonNegativeInteger(stringIn in varchar2) return boolean;
  function isANumber(stringIn in varchar2,
                     allowDecimalsIn in boolean default true,
                     allowNegativesIn in boolean default true) return boolean;
  function isArgumentTooLong(tableNameIn in varchar2,
                             columnNameIn in varchar2,
                             argumentIn in varchar2) return boolean;
  function isConversionTypeValid(conversionTypeIn in varchar2) return boolean;
  function isCurrencyCodeValid(currencyCodeIn in varchar2) return boolean;
  function isDateInRange(currentDateIn in date default sysdate,
                         startDateIn in date,
                         endDateIn in date) return boolean;
  function longStringListsMatch(longStringList1InOut in out nocopy longStringList,
                                longStringList2InOut in out nocopy longStringList,
                                sortList1In in boolean default false,
                                sortList2In in boolean default true) return boolean;
  function longestStringListsMatch(longestStringList1InOut in out nocopy longestStringList,
                                   longestStringList2InOut in out nocopy longestStringList,
                                   sortList1In in boolean default false,
                                   sortList2In in boolean default true) return boolean;
  function matchCharacter(stringIn in varchar2,
                          locationIn in integer,
                          characterIn in varchar2) return boolean;
  function personIdToUserId(personIdIn in integer) return integer;
  function recordDelimiter return varchar2;
  /* See the comment for getCarriageReturn above for an explanation of removeReturns. */
  function removeReturns(stringIn in varchar2,
                         replaceWithSpaces in boolean default false) return varchar2;
  function removeScriptTags(stringIn in varchar2 default null) return varchar2;
  function stringListsMatch(stringList1InOut in out nocopy stringList,
                            stringList2InOut in out nocopy stringList,
                            sortList1In in boolean default false,
                            sortList2In in boolean default true) return boolean;
  function userIdToPersonId(userIdIn in integer) return integer;
  function useWorkflow(transactionIdIn in varchar2 default null,
                       applicationIdIn in integer) return boolean;
  function validateUser(responsibilityIn in integer,
                        applicationIdIn in integer default null) return integer;
  /*
    versionDateToDisplayDate transforms a date-string with the format versionDateFormatModel
    to a date-string formatted per the end user's preferences by fnd_date.date_to_displayDate.
  */
  function versionDateToDisplayDate(stringDateIn in varchar2) return varchar2;
  /* versionDateToString transforms a date to a date-string with the format versionDateFormatModel. */
  function versionDateToString(dateIn in date) return varchar2;
  /* versionStringToDate transforms a date-string with the format versionDateFormatModel to a date. */
  function versionStringToDate(stringDateIn in varchar2) return date;
  procedure appendRuleIdToSource(ruleIdIn in integer,
                                 sourceInOut in out nocopy varchar2);
  /* Translation Routines for approverRecord- approverRecord2*/
  procedure apprRecordToApprRecord2(approverRecordIn in ame_util.approverRecord,
                                    itemIdIn in varchar2 default null,
                                    approverRecord2Out out nocopy ame_util.approverRecord2);
  procedure apprRecord2ToApprRecord(approverRecord2In in ame_util.approverRecord2,
                                    approverRecordOut out nocopy ame_util.approverRecord);
  procedure apprTableToApprTable2(approversTableIn in ame_util.approversTable,
                                  itemIdIn in varchar2 default null,
                                  approversTable2Out out nocopy ame_util.approversTable2) ;
  procedure apprTable2ToApprTable(approversTable2In in ame_util.approversTable2,
                                  approversTableOut out nocopy ame_util.approversTable);
  procedure checkForSqlInjection(queryStringIn in varchar2);
  procedure compactIdList(idListInOut in out nocopy idList);
  procedure compactLongStringList(longStringListInOut in out nocopy ame_util.longStringList);
  procedure compactLongestStringList(longestStringListInOut in out nocopy ame_util.longestStringList);
  procedure compactStringList(stringListInOut in out nocopy ame_util.stringList) ;
  procedure convertApproversTableToValues(approversTableIn in ame_util.approversTable,
                                          personIdValuesOut out nocopy ame_util.idList,
                                          userIdValuesOut out nocopy ame_util.idList,
                                          apiInsertionValuesOut out nocopy ame_util.charList,
                                          authorityValuesOut out nocopy ame_util.charList,
                                          approvalTypeIdValuesOut out nocopy ame_util.idList,
                                          groupOrChainIdValuesOut out nocopy ame_util.idList,
                                          occurrenceValuesOut out nocopy ame_util.idList,
                                          sourceValuesOut out nocopy ame_util.longStringList,
                                          statusValuesOut out nocopy ame_util.stringList);
  procedure convertApproversTable2ToValues(approversTableIn in ame_util.approversTable2,
                                           namesOut out nocopy ame_util.longStringList,
                                           itemClassesOut out nocopy ame_util.stringList,
                                           itemIdsOut out nocopy ame_util.stringList,
                                           apiInsertionsOut out nocopy ame_util.charList,
                                           authoritiesOut out nocopy ame_util.charList,
                                           actionTypeIdsOut out nocopy ame_util.idList,
                                           groupOrChainIdsOut out nocopy ame_util.idList,
                                           occurrencesOut out nocopy ame_util.idList,
                                           approverCategoriesOut out nocopy ame_util.charList,
                                           statusesOut out nocopy ame_util.stringList);
  procedure convertValuesToApproversTable(personIdValuesIn in ame_util.idList,
                                          userIdValuesIn in ame_util.idList,
                                          apiInsertionValuesIn in ame_util.charList,
                                          authorityValuesIn in ame_util.charList,
                                          approvalTypeIdValuesIn in ame_util.idList,
                                          groupOrChainIdValuesIn in ame_util.idList,
                                          occurrenceValuesIn in ame_util.idList,
                                          sourceValuesIn in ame_util.longStringList,
                                          statusValuesIn in ame_util.stringList,
                                          approversTableOut out nocopy ame_util.approversTable);
  procedure convertValuesToApproversTable2(nameValuesIn in ame_util.longStringList,
                                          approverCategoryValuesIn in ame_util.charList,
                                          apiInsertionValuesIn in ame_util.charList,
                                          authorityValuesIn in ame_util.charList,
                                          approvalTypeIdValuesIn in ame_util.idList,
                                          groupOrChainIdValuesIn in ame_util.idList,
                                          occurrenceValuesIn in ame_util.idList,
                                          sourceValuesIn in ame_util.longStringList,
                                          statusValuesIn in ame_util.stringList,
                                          approversTableOut out nocopy ame_util.approversTable2);
  procedure copyApproverRecord2(approverRecord2In in approverRecord2,
                                approverRecord2Out out nocopy approverRecord2);
  procedure copyApproversTable2(approversTable2In in approversTable2,
                                approversTable2Out out nocopy approversTable2);
  procedure copyCharList(charListIn in charList,
                         charListOut out nocopy charList);
  procedure copyIdList(idListIn in idList,
                       idListOut out nocopy idList);
  procedure copyLongStringList(longStringListIn in longStringList,
                               longStringListOut out nocopy longStringList);
  procedure copyStringList(stringListIn in stringList,
                           stringListOut out nocopy stringList);
  procedure deserializeLongStringList(longStringListIn in varchar2,
                                      longStringListOut out nocopy longStringList);
  procedure getAllowedAppIds(applicationIdsOut out nocopy ame_util.stringList,
                             applicationNamesOut out nocopy ame_util.stringList);
  procedure getApplicationList(applicationListOut out nocopy idStringTable);
  procedure getApplicationList2(applicationIdListOut out nocopy stringList,
                                applicationNameListOut out nocopy stringList);
  procedure getApplicationList3(applicationIdIn in integer,
                                applicationIdListOut out nocopy stringList,
                                applicationNameListOut out nocopy stringList);
  procedure getConversionTypes(conversionTypesOut out nocopy ame_util.stringList);
  procedure getCurrencyCodes(currencyCodesOut out nocopy ame_util.stringList);
  procedure getCurrencies(currencyCodesOut out nocopy ame_util.stringList,
                          currencyNamesOut out nocopy ame_util.stringList);
  procedure getFndApplicationId(applicationIdIn in integer,
                                fndApplicationIdOut out nocopy integer,
                                transactionTypeIdOut out nocopy varchar2);
  procedure getWorkflowAttributeValues(applicationIdIn in integer,
                                       transactionIdIn in varchar2,
                                       workflowItemKeyOut out nocopy varchar2,
                                       workflowItemTypeOut out nocopy varchar2);
  procedure identArrToIdList(identArrIn in owa_util.ident_arr,
                             startIndexIn in integer default 2,
                             idListOut out nocopy idList);
  procedure identArrToLongestStringList(identArrIn in owa_util.ident_arr,
                                        startIndexIn in integer default 2,
                                        longestStringListOut out nocopy longestStringList);
  procedure identArrToStringList(identArrIn in owa_util.ident_arr,
                                 startIndexIn in integer default 2,
                                 stringListOut out nocopy stringList);
  procedure idListToStringList(idListIn in idList,
                               stringListOut out nocopy stringList);
  /* Translation Routines for insertionRecord - insertionRecord2 */
   procedure insTable2ToInsTable(insertionsTable2In in ame_util.insertionsTable2,
                                 insertionsTableOut out nocopy ame_util.insertionsTable) ;
   procedure insTableToInsTable2(insertionsTableIn in ame_util.insertionsTable,
                                 transactionIdIn in varchar2,
                                 insertionsTable2Out out nocopy ame_util.insertionsTable2) ;
  procedure makeEven(numberInOut in out nocopy integer);
  procedure makeOdd(numberInOut in out nocopy integer);
  /* Translation Routines  orderRecord - insertionRecord2 */
   procedure ordRecordToInsRecord2(orderRecordIn in ame_util.orderRecord,
                                 transactionIdIn in varchar2,
                                 approverIn in ame_util.approverRecord,
                                 insertionRecord2Out out nocopy ame_util.insertionRecord2) ;

  /*
    An ame_util.approverRecord2's source field is either a field-delimited list of
    rule IDs, or a string representing any of several insertion types.  In all cases,
    parseSourceValue sets sourceDescriptionOut to a string describing the source.  In
    the case of a rule-generated approver, parseSourceValue populates ruleIdListOut
    with the rule IDs requiring the approver.
  */
  procedure parseSourceValue(sourceValueIn in varchar2,
                             sourceDescriptionOut out nocopy varchar2,
                             ruleIdListOut out nocopy ame_util.idList);
  procedure parseStaticCurAttValue(applicationIdIn in integer,
                                   attributeIdIn in integer,
                                   attributeValueIn in varchar2,
                                   localErrorIn in boolean,
                                   amountOut out nocopy varchar2,
                                   currencyOut out nocopy varchar2,
                                   conversionTypeOut out nocopy varchar2);
  procedure purgeOldTempData;
  procedure purgeOldTempData2(errbuf out nocopy varchar2,
                              retcode out nocopy varchar2);
  procedure purgeOldTransLocks(errbuf out nocopy varchar2,
                               retcode out nocopy varchar2);
  procedure runtimeException(packageNameIn in varchar2,
                             routineNameIn in varchar2,
                             exceptionNumberIn in integer,
                             exceptionStringIn in varchar2);
  procedure serializeApprovers(approverNamesIn in ame_util.longStringList,
                               approverDescriptionsIn in ame_util.longStringList,
                               maxOutputLengthIn in integer,
                               approverNamesOut out nocopy varchar2,
                               approverDescriptionsOut out nocopy varchar2);
  procedure setConfigVar(variableNameIn  in varchar2,
                         variableValueIn in varchar2,
                         applicationIdIn in integer default null);
/*
AME_STRIPING
  procedure setCurrentStripeSetId(applicationIdIn in integer,
                                  stripeSetIdIn in integer);
*/
  procedure setTransTypeCookie(applicationIdIn in integer);
  procedure sortIdListInPlace(idListInOut in out nocopy idList);
  procedure sortLongStringListInPlace(longStringListInOut in out nocopy longStringList);
  procedure sortLongestStringListInPlace(longestStringListInOut in out nocopy longestStringList);
  procedure sortStringListInPlace(stringListInOut in out nocopy stringList);
  procedure stringListToIdList(stringListIn in stringList,
                               idListOut out nocopy idList);
  procedure substituteStrings(stringIn in varchar2,
                              targetStringsIn in ame_util.stringList,
                              substitutionStringsIn in ame_util.stringList,
                              stringOut out nocopy varchar2);
end ame_util;
/

create or replace package WF_CORE AUTHID DEFINER as
/* $Header: wfcores.pls 120.15.12020000.5 2017/01/04 06:59:24 nsanika ship $ */
/*#
 * Provides APIs that can be called by an application
 * program or workflow function in the runtime phase
 * to handle error processing.
 * @rep:product OWF
 * @rep:displayname Workflow Core
 * @rep:lifecycle active
 * @rep:compatibility S
 * @rep:category BUSINESS_ENTITY WF_ENGINE
 * @rep:ihelp FND/@wfcore See the related online help
 */

--
-- CONN_TAG_WF
--  This is the connection tag identificator for workflow module
-- CONN_TAG_BES
--  This is the connection tag identificator for business events
CONN_TAG_WF varchar2(2):='wf';
CONN_TAG_BES varchar2(3):='bes';


--
-- SESSION_LEVEL
--   The protection level at which this session is operating
--
session_level   NUMBER := 10;

--
-- UPLOAD_MODE
--   Mode to upload data
-- Valid values are:
--   UPGRADE - honor both protection and customization levels of data
--   UPLOAD - honor only protection level of data
--   FORCE - force upload regardless of protection or customization level
--
upload_mode VARCHAR2(8) := 'UPGRADE';

--
-- ERROR_XXX - error message variables
--   When a workflow error occurs, these variables will be populated
--   with all available information about the problem
--
error_name      VARCHAR2(30);
error_number    NUMBER;
error_message   VARCHAR2(2000);
error_stack     VARCHAR2(32000);



/*
** Create a global plsql variable that stores the current item
** type when uploading an item.  This is used by the generic
** loader overlay because the primary key of the wf_item_types
** table is :NAME and the primary key for the wf_item_attributes
** table is :NAME and item_type but the item_type comes from th
** :NAME value in the loader definition
*/
upload_placeholder    VARCHAR2(30) := NULL;

-- Local_CS
--
-- Local CharacterSet
LOCAL_CS        VARCHAR2(30) := NULL;

-- Newline in local CharacterSet
LOCAL_CS_NL     VARCHAR2(30) := NULL;

-- Tab in local CharacterSet
LOCAL_CS_TB     VARCHAR2(30) := NULL;

-- Carriage Return in local CharacterSet
LOCAL_CS_CR     VARCHAR2(30) := NULL;

-- Bug 3945469
-- Create two global plsql variables to store the database major version
--   and the value of aq_tm_processes
G_ORACLE_MAJOR_VERSION	NUMBER;
G_AQ_TM_PROCESSES	VARCHAR2(512) ;

--
-- Canonical Format Masks
--
-- Copied from FND_NUMBER and FND_DATE packages.
--
canonical_date_mask VARCHAR2(26) := 'YYYY/MM/DD HH24:MI:SS';
canonical_number_mask VARCHAR2(100) := 'FM999999999999999999999.99999999999999999999';

/*
** Implements the Hash Key Method
*/
HashBase               NUMBER := 1;
HashSize               NUMBER := 16777216;  -- 2^24

-- HashKey
-- Generate the Hash Key for a string
FUNCTION HashKey (p_HashString in varchar2) return number;

--
-- Clear
--   Clear the error buffers.
-- EXCEPTIONS
--   none
--
/*#
 * Clears the error buffer.
 * @rep:lifecycle active
 * @rep:displayname Clear
 * @rep:ihelp FND/@wfcore#a_clear See the related online help
 */
procedure Clear;
pragma restrict_references(CLEAR, WNDS, RNDS, RNPS);

--
-- Get_Error
--   Return current error info and clear error stack.
--   Returns null if no current error.
--
-- IN
--   maxErrStackLength - Maximum length of error_stack to return - number
--
-- OUT
--   error_name - error name - varchar2(30)
--   error_message - substituted error message - varchar2(2000)
--   error_stack - error call stack, truncated if needed  - varchar2(2000)
-- EXCEPTIONS
--   none
--
/*#
 * Returns the internal name of the current error message
 * and the token substituted error message. The procedure
 * also clears the error stack. A null value is returned
 * if there is no current error.
 * @param err_name Error Name
 * @param err_message Error Message
 * @param err_stack Error Stack
 * @param maxErrStackLength Maximum length of error stack to return
 * @rep:lifecycle active
 * @rep:displayname Get Error
 * @rep:ihelp FND/@wfcore#a_geterr See the related online help
 */
procedure Get_Error(err_name out nocopy varchar2,
                    err_message out nocopy varchar2,
                    err_stack out nocopy varchar2,
                    maxErrStackLength in number default 4000);
pragma restrict_references(GET_ERROR, WNDS, RNDS);

--
-- Token
--   define error token
-- IN
--   token_name  - name of token
--   token_value - token value
-- EXCEPTIONS
--   none
--
/*#
 * Defines an error token and substitutes it with a value
 * for use in a predefined workflow error message.
 * @param token_name Token  Name
 * @param token_value Token Value
 * @rep:lifecycle active
 * @rep:displayname Token
 * @rep:ihelp FND/@wfcore#a_token See the related online help
 */
procedure Token(token_name  in varchar2,
                token_value in varchar2);
pragma restrict_references(TOKEN, WNDS, RNDS);

--
-- Substitute
--   Return substituted message string, with exception if not found
-- IN
--   mtype - message type (WFERR, WFTKN, etc)
--   mname - message internal name
-- EXCEPTIONS
--   Raises an exception if message is not found.
--
function Substitute(mtype in varchar2, mname in varchar2)
return varchar2;

--
-- Translate
--   Get substituted message string
-- IN
--   tkn_name - Message name (must be WFTKN)
-- RETURNS
--   Translated value of string token
--
/*#
 * Translates the string value of an error token
 * by returning the language-specific value for
 * the token defined in the WF_RESOURCES table for
 * the current language setting.
 * @param tkn_name Token Name
 * @return Translated token value
 * @rep:lifecycle active
 * @rep:displayname Translate
 * @rep:ihelp FND/@wfcore#a_transl See the related online help
 */
function Translate (tkn_name in varchar2)
return varchar2;
pragma restrict_references(TRANSLATE, WNDS);

--
-- Raise
--   Raise an exception to the caller
-- IN
--   error_name - error name (must be WFERR)
-- EXCEPTIONS
--   Raises an a user-defined (20002) exception with the error message.
--
/*#
 * Raises a predefined workflow exception to the calling
 * application by supplying a correct error number and
 * token substituted message for the specified internal
 * error message name.
 * @param name  Name
 * @rep:lifecycle active
 * @rep:displayname Raise
 * @rep:ihelp FND/@wfcore#a_raise See the related online help
 */
procedure Raise(name in varchar2);

--
-- Context
--   set procedure context (for stack trace)
-- IN
--   pkg_name   - package name
--   proc_name  - procedure/function name
--   arg1       - first IN argument
--   argn       - n'th IN argument
-- EXCEPTIONS
--   none
--
/*#
 * Adds an entry to the error stack to provide context
 * information that helps locate the source of an error.
 * Use this procedure with predefined errors raised by
 * calls to TOKEN( ) and RAISE( ), with custom-defined
 * exceptions, or even without exceptions whenever an error
 * condition is detected.
 * @param pkg_name Package  Name
 * @param proc_name Procedure Name
 * @param arg1  Argument 1
 * @param arg2  Argument 2
 * @param arg3  Argument 3
 * @param arg4  Argument 4
 * @param arg5  Argument 5
 * @param arg6  Argument 6
 * @param arg7  Argument 7
 * @param arg8  Argument 8
 * @param arg9  Argument 9
 * @param arg10 Argument 10
 * @rep:lifecycle active
 * @rep:displayname Context
 * @rep:ihelp FND/@wfcore#a_context See the related online help
 */
procedure Context(pkg_name  in varchar2,
                  proc_name in varchar2,
                  arg1      in varchar2 default '*none*',
                  arg2      in varchar2 default '*none*',
                  arg3      in varchar2 default '*none*',
                  arg4      in varchar2 default '*none*',
                  arg5      in varchar2 default '*none*',
                  arg6      in varchar2 default '*none*',
                  arg7      in varchar2 default '*none*',
                  arg8      in varchar2 default '*none*',
                  arg9      in varchar2 default '*none*',
                  arg10      in varchar2 default '*none*');
pragma restrict_references(CONTEXT, WNDS);

--
-- RANDOM
--   Return a random string
-- RETURNS
--   A random string, max 80 characters
--
function RANDOM
return varchar2;
-- pragma restrict_references(RANDOM, WNDS);

--
-- ACTIVITY_RESULT
--	Return the meaning of an activities result_type
--	Including standard engine codes
-- IN
--   LOOKUP_TYPE
--   LOOKUP_CODE
--
-- RETURNS
--   MEANING
--
function activity_result( result_type in varchar2, result_code in varchar2) return varchar2;
pragma restrict_references(ACTIVITY_RESULT, WNDS, WNPS, RNPS);
--
--
--
-- GetResource
--   Called by WFResourceManager.class. Used by the Monitor and Lov Applet.
--   fetch A resource from wf_resource table.
-- IN
-- x_restype
-- x_resname

procedure GetResource(x_restype varchar2,
                      x_resname varchar2);
--
-- GetResources
--   Called by WFResourceManager.class. Used by the Monitor and Lov Applet.
--   fetch some resources from wf_resource table that match the respattern.
-- IN
-- x_restype
-- x_respattern
procedure GetResources(x_restype varchar2,
                       x_respattern varchar2);

-- *** Substitue HTML Characters ****
--function SubstituteSpecialChars

/*#
 * Substitutes HTML character entity references for special characters in
 * a text string and returns the modified text including the substitutions.
 * You can use this function as a security precaution when creating a PL/SQL
 * document or a PL/SQL CLOB document that contains HTML, to ensure that only
 * the HTML code you intend to include is executed.
 * @param some_text Text string with HTML characters
 * @return String with HTML characters substituted with HTML codes
 * @rep:lifecycle active
 * @rep:displayname Substitute HTML Special Characters
 * @rep:compatibility S
 * @rep:ihelp FND/@a_subsc See the related online help
 */
function SubstituteSpecialChars(some_text in varchar2)
return varchar2;
pragma restrict_references(SubstituteSpecialChars,WNDS);

-- *** Special Char functions ***

-- Local_Chr
--   Return specified character in current codeset
-- IN
--   ascii_chr - chr number in US7ASCII
function Local_Chr(
  ascii_chr in number)
return varchar2;
pragma restrict_references (LOCAL_CHR, WNDS);

-- Newline
--   Return newline character in current codeset
function Newline
return varchar2;
pragma restrict_references (NEWLINE, WNDS);

-- Tab
--   Return tab character in current codeset
function Tab
return varchar2;
pragma restrict_references (TAB, WNDS);

-- CR - CarriageReturn
--   Return CR character in current codeset.
function CR
return varchar2;

--
-- CheckIllegalChars (PRIVATE)
-- IN
--   p_text - text to be checked
--   p_raise_exception - raise exception if true
-- RET
--   Return true if illegal character exists
function CheckIllegalChars(p_text varchar2, p_raise_exception boolean, p_illegal_charset varchar2 default null)
return boolean;

procedure InitCache;

  -- Bug 7578908. Phase 1 default values for NLS parameters
  -- Strictly to be used by WF only.
  FUNCTION nls_date_format   RETURN varchar2;
  FUNCTION nls_date_language RETURN varchar2;
  FUNCTION nls_calendar      RETURN varchar2;
  FUNCTION nls_sort         RETURN varchar2;
  FUNCTION nls_currency      RETURN varchar2;
  FUNCTION nls_numeric_characters RETURN varchar2;
  FUNCTION nls_language RETURN varchar2;
  FUNCTION nls_territory RETURN varchar2;
  procedure initializeNLSDefaults;

  FUNCTION client_timezone      RETURN varchar2;    --- Bug 19338480 for WF TZ ER

  --
  -- Tag_DB_Session (PRIVATE)
  -- Used by the different WF Engine entry points to tag the current session
  -- as per the Connection tag initiative described in bug 9370420
  -- This procedure checks for the user and application id. If they are not
  -- set then it means the context is not set.
  --
  procedure TAG_DB_SESSION(p_module_type varchar, p_action varchar2);

  /*
  ** PUBLIC
  ** get_database_default_edition - this function retrieves the database's
  ** default edition from dictionary view DATABASE_PROPERTIES so that both
  ** Java and PLSQL BES determines how to process business event systems
  ** raised during patching time
  */
  function database_default_edition return varchar2;

  /*
  ** PUBLIC
  ** database_current_edition - this function retrieves the current session's
  ** edition from sys_context('userenv', 'CURRENT_EDITION_NAME') for
  ** Java and PLSQL BES to determine how to process business event systems
  ** raised during patching time
  */
  function database_current_edition return varchar2;

--
-- getDedicatedComponentsCorrIds
-- ER 16593551: Gets the correlation Ids of all dedicated components belongs
-- to specified agent and return as comma separated values
-- IN
--   p_agent_name  -- agent name
--   p_schemaName  -- schema name
-- RETURNS
--   boolean
--
  function getDedicatedComponentsCorrIds(p_agent_name in varchar2,
                                         p_schemaName in varchar2)
  return varchar2 RESULT_CACHE;

--
-- matchCorrId
-- ER 16593551: Checks that the given message corrId matches with the any one of the
-- dedicated components corrId list and returns 0 if it matches, otherwise returns 1
-- IN
--   p_msgCorrId    -- message correlation Id
--   p_corrId_list  -- dedicated components correlation Ids list
-- RETURNS
--   number
--
function matchCorrId(p_msgCorrId in varchar2,
	               p_corrId_list in varchar2)
return number RESULT_CACHE;


end WF_CORE;
/

create table wf_roles(ORIG_SYSTEM_ID  number);
create table wf_users(name VARCHAR2(320));